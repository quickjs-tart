#!/bin/bash
set -eu

if [ -z ${1+x} ]; then
  echo "usage: $0 <ANDROID_ABI>"
  exit 1
fi

ABI=$1

case $ABI in
armeabi-v7a)
  ARCH="arm"
  BUILD_DIR="build-android-armeabi-v7a"
  CPU_FAMILY=arm
  CPU=armv7
  CLANG_NAME=armv7a-linux-androideabi24
  TOOLS_NAME=arm-linux-androideabi
  ;;
arm64-v8a)
  ARCH="arm64"
  BUILD_DIR="build-android-arm64-v8a"
  CPU_FAMILY=aarch64
  CPU=armv8
  CLANG_NAME=aarch64-linux-android24
  TOOLS_NAME=aarch64-linux-android
  ;;
x86)
  ARCH="x86"
  BUILD_DIR="build-android-x86"
  CPU_FAMILY=x86
  CPU=x86
  CLANG_NAME=i686-linux-android24
  TOOLS_NAME=i686-linux-android
  ;;
x86_64)
  ARCH="x86_64"
  BUILD_DIR="build-android-x86_64"
  CPU_FAMILY=x86_64
  CPU=x86_64
  CLANG_NAME=x86_64-linux-android24
  TOOLS_NAME=x86_64-linux-android
  ;;
*)
  echo unknown ABI
  exit 1
  ;;
esac

API_LEVEL="24"

ANDROID_NDK_HOME="${ANDROID_SDK_ROOT}/ndk-bundle"

HOST_ROOT="${ANDROID_NDK_HOME}/toolchains/llvm/prebuilt/linux-x86_64"
SYS_ROOT="${HOST_ROOT}/sysroot"
LIB_PATH="${SYS_ROOT}/usr/lib/${ARCH}-linux-androideabi:${SYS_ROOT}/usr/lib/${ARCH}-linux-androideabi/${API_LEVEL}:${ANDROID_NDK_HOME}/platforms/android-${API_LEVEL}/arch-${ARCH}/usr/lib"
INC_PATH="${SYS_ROOT}/usr/include"

export PATH="${HOST_ROOT}/bin:${PATH}"

export CFLAGS="\
  -DANDROID_STL=none \
  -DANDROID_TOOLCHAIN=clang \
  -DANDROID_PLATFORM=android-${API_LEVEL} \
  -DANDROID_ABI=${ABI}"

echo "building..."

rm -rf ${BUILD_DIR}

CROSSFILE=cross/android-$ABI.cross.txt

cat > $CROSSFILE <<EOF
[binaries]
c = '$CLANG_NAME-clang'
cmake = 'false'
ar = 'llvm-ar'
as = '$TOOLS_NAME-as'
ranlib = '$TOOLS_NAME-ranlib'
ld = '$TOOLS_NAME-ld'
strip = '$TOOLS_NAME-strip'
pkgconfig = 'false'

[properties]
needs_exe_wrapper = 'false'

[host_machine]
system = 'android'
cpu_family = '$CPU_FAMILY'
cpu = '$CPU'
endian = 'little'
EOF

export LDFLAGS=-flto
export CFLAGS=-flto

meson setup --errorlogs  \
  -Doptimization=3 \
  --prefix=${ANDROID_NDK_HOME} \
  --includedir=${INC_PATH} \
  --libdir=${LIB_PATH} \
  --build.cmake-prefix-path=${SYS_ROOT} \
  --cross-file $CROSSFILE  \
  ${BUILD_DIR} .

ninja -v -C ${BUILD_DIR}
