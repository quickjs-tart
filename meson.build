project('quickjs-tart', 'c',
  version : '0.0.1',
  meson_version: '>=0.6.1',
  default_options : [
    'warning_level=0',
    'optimization=2',
  ],
)

flags = [
  '-D_GNU_SOURCE',
  '-D_LARGEFILE_SOURCE',
  '-D_FILE_OFFSET_BITS=64',
  '-DCONFIG_VERSION="0.0.1"',
  '-DCONFIG_BIGNUM',
  '-fno-omit-frame-pointer',
]

add_project_arguments(flags, language : 'c')
add_project_arguments(flags, language : 'c', native : true)

if host_machine.system() == 'android'
  add_project_link_arguments('-Wl,--hash-style=both', language : 'c')
endif


cc = meson.get_compiler('c')

m_dep = cc.find_library('m', required : false)
dl_dep = cc.find_library('dl', required : true)
thread_dep = dependency('threads')

# Async DNS resolver library
cares_proj = subproject('c-ares', required : true)

# TLS library
mbedtls_proj = subproject('mbedtls', required : true)
#mbedcrypto_dep = cc.find_library('mbedcrypto', required : true)
mbedcrypto_dep = mbedtls_proj.get_variable('mbedcrypto_dep')
mbedtls_dep = mbedtls_proj.get_variable('mbedtls_dep')
mbedx509_dep = mbedtls_proj.get_variable('mbedx509_dep')

# Library for HTTP requests
curl_proj = subproject('curl', required : true)
# curl_dep = cc.find_library('curl', required : true)
curl_dep = curl_proj.get_variable('curl_dep')

# Crypto library
sodium_proj = subproject('libsodium', required : true)
#sodium_dep = cc.find_library('sodium', required : true)
sodium_dep = sodium_proj.get_variable('sodium_dep')

# quickjs math library (big float)
libbf = static_library('bf', 'quickjs/libbf.c')
# regular expression library
libregexp = static_library('regexp', 'quickjs/libregexp.c')
# unicode
libunicode = static_library('unicode', 'quickjs/libunicode.c')
# general utilities
cutils = static_library('cutils', 'quickjs/cutils.c')
# standard library for quickjs (std and os modules)
quickjs_libc = static_library('quickjs-libc', ['quickjs/quickjs-libc.c', 'quickjs/quickjs-http.c'], dependencies : curl_dep )
# base JS interpreter
quickjs = static_library('quickjs', 'quickjs/quickjs.c')
sqlite3 = static_library('sqlite3', 'sqlite3/sqlite3.c')

# avoid warning but compile more slowly on non-cross builds
avoid_cross_warning = true

# native version of quickjs used by the qjsc (.js -> .c compiler),
# compiled for the build platform.
if avoid_cross_warning or meson.is_cross_build()
  libbf_native = static_library('bf_native', 'quickjs/libbf.c', native : true)
  libregexp_native = static_library('regexp_native', 'quickjs/libregexp.c', native : true)
  libunicode_native = static_library('unicode_native', 'quickjs/libunicode.c', native : true)
  cutils_native = static_library('cutils_native', 'quickjs/cutils.c', native : true)
  quickjs_native = static_library('quickjs_native', 'quickjs/quickjs.c', native : true)
else
  libbf_native = libbf
  libregexp_native = libregexp
  libunicode_native = libunicode
  cutils_native = cutils
  quickjs_libc_native = quickjs_libc
  quickjs_native = quickjs
endif

# QuickJS compiler (.js -> C)
qjsc_exe = executable('qjsc', [
  'quickjs/qjsc.c',
  'quickjs/quickjs-libc.c',
  ],
  # Just the compiler, no HTTP support required
  c_args : ['-DNO_HTTP'],
  link_with: [
    libbf_native, 
    libregexp_native, 
    libunicode_native,
    cutils_native,
    quickjs_native,
  ],
  dependencies: [
    m_dep,
    dl_dep,
    thread_dep,
  ],
  native : true)

# JS implementation of the read-eval-print loop,
# just used for interactive testing
repl_c = custom_target('repl_c',
  input : ['quickjs/repl.js'],
  output : ['repl.c'],
  command : [qjsc_exe, '-c', '-m',
    '-o', '@OUTPUT@',
    '@INPUT@'])

# Helper functions used by the wallet-core JS,
# compiled to C.
prelude_c = custom_target('prelude_c',
  input : ['prelude.js'],
  output : ['prelude.c'],
  command : [qjsc_exe, '-c', '-m', '-M', 'tart',
    '-o', '@OUTPUT@',
    '@INPUT@'])

# Wallet core JS file,
# compiled to C.
wallet_core_c = custom_target('wallet_core_c',
  input : ['taler-wallet-core-qjs.mjs'],
  output : ['wallet_core.c'],
  command : [qjsc_exe, '-c', '-m', '-M', 'tart', '-N', 'qjsc_wallet_core',
    '-o', '@OUTPUT@',
    '@INPUT@'])

# Static libraries from the generated C files
repl = static_library('repl', repl_c)
prelude = static_library('prelude', prelude_c)
wallet_core = static_library('wallet_core', wallet_core_c)

# Taler runtime ("tart") loadable JavaScript module
tart = static_library('tart', 'tart_module.c',
  link_with : [sqlite3],
  dependencies : [
    m_dep,
    mbedcrypto_dep,
    mbedtls_dep,
    mbedx509_dep,
    curl_dep,
    sodium_dep
  ])

# Interactive JS interpreter with Taler-specific
# functions (crypto etc.)
qtart_exe = executable('qtart', [
  'qtart.c',
  ],
  link_with: [
    libbf, 
    libregexp, 
    libunicode ,
    cutils,
    quickjs_libc,
    quickjs,
    repl,
    wallet_core,
    prelude,
    tart,
  ],
  dependencies: [
    m_dep,
    mbedcrypto_dep,
    mbedtls_dep,
    mbedx509_dep,
    curl_dep,
    sodium_dep
  ])

# Shared library that implements wallet-core, typically
# the only build output directly used by the UI layer.
talerwalletcore_lib = shared_library('talerwalletcore', 'taler_wallet_core_lib.c',
  link_with : [
    libbf, 
    libregexp, 
    libunicode ,
    cutils,
    quickjs_libc,
    quickjs,
    tart,
    wallet_core,
    prelude,
  ],
  dependencies: [
    m_dep,
    mbedcrypto_dep,
    mbedtls_dep,
    mbedx509_dep,
    curl_dep,
    sodium_dep
  ])

# Example for using libtalerwalletcore.so
wallet_client_example_exe = executable('wallet-client-example',
  'wallet-client-example.c',
  link_with : [talerwalletcore_lib])
