# QuickJS Taler Runtime (qtart)

This repository contains qtart, a JavaScript runtime for the GNU Taler wallet
based on Fabrice Bellard's QuickJS. The runtime is statically linked.

## Building

Prerequisite dependencies:
- `wget`
- `meson`
- `ninja`
- `gcc` or `clang`

Building also requires the `taler-wallet-core-qjs.mjs` file. You can
either build it yourself or download if from `taler.net` via the `./download_wallet_core_js.sh`
script.

The following commands build the project and place resulting files
in the `build/` directory:

```
$ meson setup build
$ meson compile -C build
```

## Architecture

Infra:

- The `tart` native quickjs module provides Taler-specific runtime functionalty for the wallet.
- The `std` and `os` modules are extended with functionality needed by the wallet,
  such as a curl-based HTTP client and file access.
- `prelude.js` adapts the global environment to enable the wallet JS to run
- `taler-wallet-core-qjs.mjs` is the bundled taler-wallet-core.

Programs/interfaces:
- `$builddir/qtart` provides a JS interpreter and REPL with a bundled taler-wallet-core.
  It is mainly used for testing and benchmarking.
- `$builddir/libtalerwalletcore.so` 
- `taler_wallet_core_lib.h` declares the symbols exported by `libtalerwalletcore.so`
- `wallet-client-example.c` is an example client for `libtalerwalletcore.so`

## To Do

- curl is not used asynchronously, but HTTP requests block
- TLS certificate verification is disabled, because some platforms (Apple!)
  don't have trusted CAs in the file system.  Eventually, we will
  ship with our own certificate store.
- We might leak memory in some places

## Repository Structure

- `quickjs/`: A slightly modified version of Fabrice Bellard's QuickJS interpreter.
We try to keep modifications as small as possible in this part of the code.
- `subprojects/`: Vendored dependencies.  Only the build systems of these dependencies
should be modified, not any of the code, if at all possible.

## Usage / Examples

Run test with `localhost` deployment:

```
./qtart -e 'testWithLocal()'
```

Run test with `taler.net` deployment:

```
./qtart -e 'testWithGv()'
```
