function *foo() {
  let x = 42;
  console.log("first yield");
  yield () => x;
  console.log("second yield");
  yield v => { x = v; };
  console.log("my x", x);
}

const g = foo();

const fun1 = g.next().value;
const fun2 = g.next().value;

console.log(fun1());
fun2(41);
console.log(fun1());
g.next();
