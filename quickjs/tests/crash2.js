import * as std from "std";

let listener;

async function waitUntilDone(ws) {
  let p2 = {};
  listener = (yn) => {
    return p2;
  };
  p2.promise = new Promise(() => {});
  await p2.promise;
}

waitUntilDone();

console.log("---- calling gc");
std.gc();
console.log("---- calling listener");
listener();
