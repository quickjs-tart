// Running this file crashes vanilla quickjs-2021-03-27
// due to a use-after-free bug.
// That happens because innerFun keeps a reference
// to the local variable p, but p gets erroneously
// garbage-collected.
// The outerFun infinitely waits for the promise
// to finish. Thus, its local variables are kept on the
// stack. Thus innerFun doesn't directly reference
// outerFun, and outerFun gets collected
// by the cycle collector.

import * as os from "os";
import * as std from "std";

let f;

let promise = new Promise(() => {});

let outerFun = async function outerFun() {
  let p = { foo: "bar" };
  function innerFun() {
    console.log(p.foo);
  }
  f = innerFun;
  // Wait indefinitely
  await promise;
}
outerFun();

// directly

//console.log("cleaning up");
//console.log("deleting promise");
//promise = undefined;
//console.log("deleting outerFun");
//outerFun = undefined;
//console.log("deleting f");
//f = undefined;
//console.log("exiting");
//std.exit(42);


// via timer
os.setTimeout(() => { console.log("calling GC"); std.gc(); f(); }, 0);
