import * as std from "std";

async function waitUntilDone(ws) {
  let p = {};
  globalThis.escFun = function escFun() { return p; };
  p.promise = new Promise(function promIni() {});
  await p.promise;
}

waitUntilDone();

//console.log("-------- running gc1");
//std.gc();
//console.log("-------- running gc2");
//std.gc();
//console.log("-------- end");
