/*
 This file is part of GNU Taler
 Copyright (C) 2024 Taler Systems SA

 GNU Taler is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free Software
 Foundation; either version 3, or (at your option) any later version.

 GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along with
 GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */


// ## Native HTTP client library support.

// Considerations:
// - the API is designed for the HTTP client implementation
//   to run in its own thread and *not* be integrated with the
//   application's main event loop.
// - focus on small API
// - not a generic HTTP client, only supposed to serve the needs
//   of a JS runtime
// - only very tiny subset of HTTP supported
// - no request/response streaming
// - should be appropriate to implement a JS HTTP fetch function
//   in the style of WHATWG fetch
// - no focus on ABI compatibility whatsoever


#ifndef _QUICKJS_HTTP_H
#define _QUICKJS_HTTP_H

#include <stdint.h>
#include <limits.h>
#include <stddef.h>

// Forward declaration;
struct JSHttpResponseInfo;

/**
 * Callback called when an HTTP response has arrived.
 *
 * IMPORTANT: May be called from an arbitrary thread.
 */
typedef void (*JSHttpResponseCb)(void *cls, struct JSHttpResponseInfo *resp);

enum JSHttpRedirectFlag {
  /**
   * Handle redirects transparently.
   */
  JS_HTTP_REDIRECT_TRANSPARENT = 0,
  /**
   * Redirect status codes are returned to the client.
   * The client can choose to follow them manually (or not).
   */
  JS_HTTP_REDIRECT_MANUAL = 1,
  /**
   * All redirect status codes result in an error.
   */
  JS_HTTP_REDIRECT_ERROR = 2,
};

/**
 * Info needed to start a new HTTP request.
 */
struct JSHttpRequestInfo {
  /**
   * Callback called with the response for the request.
   */
  JSHttpResponseCb response_cb;

  /**
   * Closure for response_cb.
   */
  void *response_cb_cls;

  /**
   * Request URL.
   */
  const char *url;

  /**
   * Request method.
   */
  const char *method;

  /**
   * NULL-terminated array of request headers.
   */
  char **request_headers;

  /**
   * 0: Handle redirects transparently.
   * 1: Handle redirects manually.
   * 2: Redirects result in an error.
   */
  enum JSHttpRedirectFlag redirect;

  /**
   * Request timeout in milliseconds.
   *
   * When 0 is specified, the timeout is the default request
   * timeout for the platform.
   *
   * When -1 is specified, there is no timeout.  This might not be
   * supported on all platforms.
   */
  int timeout_ms;

  /**
   * Enable debug output for this request.
   */
  int debug;

  /**
   * Request body or NULL.
   */
  void *req_body;

  /**
   * Length or request body or 0.
   */
  uint32_t req_body_len;
};

/**
 * Contents of an HTTP response.
 */
struct JSHttpResponseInfo {

  /**
   * Request that this is a response to.
   *
   * (Think of the request ID like a file descriptor number).
   */
  int request_id;

  /**
   * HTTP response status code or 0 on error.
   */
  int status;

  /**
   * When status is 0, error message.
   */
  char *errmsg;

  /**
   * Array of `num_response_headers` response headers.
   */
  char **response_headers;

  /**
   * Number of response headers.
   */
  int num_response_headers;

  /**
   * Response body or NULL.
   */
  void *body;

  /**
   * Length of the response body or 0.
   */
  uint32_t body_len;
};

/**
 * Callback called when an HTTP response has arrived.
 *
 * IMPORTANT: May be called from an arbitrary thread.
 */
typedef void (*JSHttpResponseCb)(void *cls, struct JSHttpResponseInfo *resp);

/**
 * Function to create a new HTTP fetch request.
 * The request can still be configured until it is started.
 * An identifier for the request will be written to @a handle.
 *
 * @return negative number on error, positive request_id on success
 */
typedef int (*JSHttpReqCreateFn)(void *cls, struct JSHttpRequestInfo *req_info);

/**
 * Cancel a request. The request_id will become invalid
 * and the callback won't be called with request_id.
 */
typedef int (*JSHttpReqCancelFn)(void *cls, int request_id);

struct JSHttpClientImplementation {
  /**
   * Opaque closure passed to client functions.
   */
  void *cls;
  JSHttpReqCreateFn req_create;
  JSHttpReqCancelFn req_cancel;
};


struct JSHttpClientImplementation *
js_curl_http_client_create(void);

void
js_curl_http_client_destroy(struct JSHttpClientImplementation *impl);

#endif /* _QUICKJS_HTTP_H */
