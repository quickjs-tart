/*
 This file is part of GNU Taler
 Copyright (C) 2024 Taler Systems SA

 GNU Taler is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free Software
 Foundation; either version 3, or (at your option) any later version.

 GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along with
 GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <curl/curl.h>
#include <arpa/inet.h>
#include <strings.h>
#include <string.h>
#include <assert.h>

#include "curl/multi.h"
#include "cutils.h"
#include "quickjs-http.h"
#include "list.h"

struct CurlClientState {
    pthread_t thread;
    pthread_mutex_t mutex;
    BOOL started;
    BOOL stopped;
    CURLSH *curlsh;
    CURLM *curlm;
    int last_request_id;
    struct list_head request_list; /* list of CurlRequestState.link */
    struct list_head add_queue;    /* multi_add_handle queue */
    struct list_head cancel_queue; /* multi_remove_handle queue */
};

struct CurlRequestState {
    struct CurlClientState *ccs;
    struct list_head link_req;    /* for request_list */
    struct list_head link_add;    /* for add_queue */
    struct list_head link_cancel; /* for cancel_queue */
    DynBuf response_data;
    BOOL cancelled;
    CURL *curl;
    int request_id;
    enum JSHttpRedirectFlag redirect;
    JSHttpResponseCb response_cb;
    void *response_cb_cls;
    // Request headers
    struct curl_slist *req_headers;
    struct curl_slist *resp_headers;
    char *errbuf;
};

// Must only be called with locked client mutex
static void destroy_curl_request_state(struct CurlRequestState *crs)
{
    struct CurlClientState *ccs;

    if (!crs) {
        return;
    }
    ccs = crs->ccs;
    crs->ccs = NULL;

    list_del(&crs->link_req);
    curl_slist_free_all(crs->req_headers);
    curl_slist_free_all(crs->resp_headers);
    dbuf_free(&crs->response_data);
    if (crs->curl) {
        curl_easy_cleanup(crs->curl);
        crs->curl = NULL;
    }
    free(crs->errbuf);
    free(crs);
}

static void *
handle_done(CURL *curl, CURLcode res)
{
    struct CurlRequestState *crs = NULL;
    struct CurlClientState *ccs = NULL;
    struct JSHttpResponseInfo hri = { 0 };
    long resp_code;
    char **headers = NULL;
    BOOL cancelled;

    curl_easy_getinfo(curl, CURLINFO_PRIVATE, &crs);
    ccs = crs->ccs;

    hri.request_id = crs->request_id;

    if (CURLE_OK == res) {
        int num_headers = 0;
        int i;
        char **headers;
        struct curl_slist *sl = crs->resp_headers;
        char *url = NULL;

        curl_easy_getinfo(curl, CURLINFO_REDIRECT_URL, &url);

        if (crs->redirect == JS_HTTP_REDIRECT_ERROR && NULL != url) {
            hri.status = 0;
            hri.errmsg = crs->errbuf;
            strncpy(crs->errbuf, "Got redirect status, but redirects are not allowed for this request", CURL_ERROR_SIZE);
            goto done;
        }

        while (sl != NULL) {
            if (NULL != strchr(sl->data, ':')) {
                num_headers++;
            }
            sl = sl->next;
        }

        headers = malloc((num_headers + 1) * sizeof(char *));
        if (!headers) {
            hri.status = 0;
            goto done;
        }
        memset(headers, 0, (num_headers + 1) * sizeof (char *));
        sl = crs->resp_headers;
        i = 0;
        while (sl != NULL) {
            if (NULL != strchr(sl->data, ':')) {
                headers[i] = sl->data;
                i++;
            }
          sl = sl->next;
        }
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &resp_code);
        hri.status = resp_code;
        hri.body = crs->response_data.buf;
        hri.body_len = crs->response_data.size;
        hri.response_headers = headers;
        hri.num_response_headers = num_headers;
    } else {
        hri.status = 0;
        hri.errmsg = crs->errbuf;
    }

done:

    pthread_mutex_lock(&ccs->mutex);
    cancelled = crs->cancelled;
    pthread_mutex_unlock(&ccs->mutex);

    if (cancelled == FALSE) {
      // FIXME: What if this CB somehow destroys the client?
      crs->response_cb(crs->response_cb_cls, &hri);
    }

    if (NULL != headers) {
        for (char **h=headers; *h; h++) {
            free(*h);
        }
        free(headers);
    }
    pthread_mutex_lock(&ccs->mutex);
    destroy_curl_request_state(crs);
    pthread_mutex_unlock(&ccs->mutex);
    return NULL;
}

static size_t curl_header_callback(char *buffer, size_t size,
                              size_t nitems, void *userdata)
{
    struct CurlRequestState *crs = userdata;
    size_t sz = size * nitems;
    char *hval;

    hval = strndup(buffer, sz);
    if (!hval) {
        return 0;
    }
    crs->resp_headers = curl_slist_append(crs->resp_headers, hval);
    free(hval);
    return sz;
}


static size_t curl_write_cb(void *data, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct CurlRequestState *rctx = userp;

    if (0 != dbuf_put(&rctx->response_data, data, realsize)) {
        return 0;
    }

    return realsize;
}


static int
create_impl(void *cls, struct JSHttpRequestInfo *req_info)
{
    struct CurlClientState *ccs = cls;
    struct CurlRequestState *crs;
    pthread_t thread;
    int res;
    CURL *curl;
    BOOL debug = req_info->debug > 0;
    const char *method = req_info->method;

    crs = malloc(sizeof *crs);
    if (!crs) {
      return -1;
    }
    memset(crs, 0, sizeof *crs);
    crs->request_id = ++ccs->last_request_id;
    crs->ccs = ccs;
    crs->response_cb = req_info->response_cb;
    crs->response_cb_cls = req_info->response_cb_cls;
    crs->errbuf = malloc(CURL_ERROR_SIZE);
    if (!crs->errbuf) {
        goto error;
    }
    memset(crs->errbuf, 0, CURL_ERROR_SIZE);
    dbuf_init(&crs->response_data);

    curl = curl_easy_init();
    crs->curl = curl;
    curl_easy_setopt(curl, CURLOPT_PRIVATE, crs);
    curl_easy_setopt(curl, CURLOPT_SHARE, ccs->curlsh);
    curl_easy_setopt(curl, CURLOPT_URL, req_info->url);
    curl_easy_setopt(curl, CURLOPT_DNS_SERVERS, "9.9.9.9");
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "qtart");
    curl_easy_setopt(curl, CURLOPT_CAINFO, "/etc/ssl/certs/ca-certificates.crt");
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, curl_header_callback);
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, crs);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_write_cb);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, crs);

    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, crs->errbuf);

    // FIXME: This is only a temporary hack until we have proper TLS CA support
    // on all platforms
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

    if (req_info->timeout_ms < 0) {
        curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 0L);
    } else if (0 == req_info->timeout_ms) {
        // Default timeout of 5 minutes.
        curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 5L * 60000L);
    } else {
        curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, (long) req_info->timeout_ms);
    }

    if (debug == TRUE) {
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    }

    crs->redirect = req_info->redirect;

    switch (req_info->redirect) {
      case JS_HTTP_REDIRECT_TRANSPARENT:
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        break;
      case JS_HTTP_REDIRECT_MANUAL:
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0L);
        break;
      case JS_HTTP_REDIRECT_ERROR:
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0L);
        break;
      default:
        assert(0);
    }

    if (0 == strcasecmp(req_info->method, "get")) {
        curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
    } else if (0 == strcasecmp(method, "delete")) {
        curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    } else if (0 == strcasecmp(method, "head")) {
        curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
    } else if ((0 == strcasecmp(method, "post")) ||
               (0 == strcasecmp(method, "put"))) {
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        if (0 == strcasecmp(method, "put")) {
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        if (req_info->req_body_len > 0) {
            curl_off_t len = req_info->req_body_len;
            curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE_LARGE, len);
            curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, req_info->req_body);
        }
    } else {
        goto error;
    }

    if (req_info->request_headers != NULL) {
      char **h = req_info->request_headers;
      while (*h) {
        crs->req_headers = curl_slist_append(crs->req_headers, *h);
        h++;
      }
    }
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, crs->req_headers);

    pthread_mutex_lock(&ccs->mutex);
    list_add_tail(&crs->link_add, &ccs->add_queue);
    list_add_tail(&crs->link_req, &ccs->request_list);
    pthread_mutex_unlock(&ccs->mutex);

    curl_multi_wakeup(ccs->curlm);

    return crs->request_id;
error:
    if (crs) {
      dbuf_free(&crs->response_data);
      if (crs->errbuf) {
        free(crs->errbuf);
      }
      if (crs->curl) {
        curl_easy_cleanup(crs->curl);
      }
      free(crs);
    }
    return -1;
}

static int
destroy_impl(void *cls, int request_id)
{
    struct list_head *el;
    struct CurlClientState *ccs = cls;  

    pthread_mutex_lock(&ccs->mutex);

    list_for_each(el, &ccs->request_list) {
        struct CurlRequestState *crs = list_entry(el, struct CurlRequestState, link_req);
        if (crs->request_id == request_id && !crs->cancelled) {
            list_add_tail(&crs->link_cancel, &ccs->cancel_queue);
        }
    }

    pthread_mutex_unlock(&ccs->mutex);

    curl_multi_wakeup(ccs->curlm);

    return 0;
}

/**
 * Entry point for the thread that processes HTTP requests with libcurl.
 */
static void *
curl_multi_thread_run(void *cls)
{
    struct CurlClientState *ccs = cls;
    struct list_head *el, *el1;
    int still_running;
    struct CURLMsg *m;
    BOOL stopped;

    while (1) {
        CURLMcode mc;

        mc = curl_multi_perform(ccs->curlm, &still_running);

        if (CURLM_OK != mc) {
            fprintf(stderr, "curl_multi_perform failed\n");
            break;
        }

        mc = curl_multi_poll(ccs->curlm, NULL, 0, 1000, NULL);
        if (CURLM_OK != mc) {
            fprintf(stderr, "curl_multi_poll failed\n");
            break;
        }

        pthread_mutex_lock(&ccs->mutex);
        stopped = ccs->stopped;
        pthread_mutex_unlock(&ccs->mutex);

        if (stopped) {
            break;
        }

        do {

            // Add new requests in queue
            pthread_mutex_lock(&ccs->mutex);
            list_for_each_safe(el, el1, &ccs->add_queue) {
                struct CurlRequestState *crs = list_entry(el, struct CurlRequestState, link_add);
                curl_multi_add_handle(ccs->curlm, crs->curl);
                list_del(el);
            }
            pthread_mutex_unlock(&ccs->mutex);

            // Cancel requests in queue
            pthread_mutex_lock(&ccs->mutex);
            list_for_each_safe(el, el1, &ccs->cancel_queue) {
                struct CurlRequestState *crs = list_entry(el, struct CurlRequestState, link_cancel);
                curl_multi_remove_handle(ccs->curlm, crs->curl);
                crs->cancelled = TRUE;
                list_del(el);
            }
            pthread_mutex_unlock(&ccs->mutex);

            // Process finished request
            int msgq = 0;
            m = curl_multi_info_read(ccs->curlm, &msgq);
            if (m && (m->msg == CURLMSG_DONE)) {
                CURL *e = m->easy_handle;
                curl_multi_remove_handle(ccs->curlm, e);
                handle_done(e, m->data.result);
            }
        } while(m);
    }
    if (CURLM_OK != curl_multi_cleanup(ccs->curlm)) {
        fprintf(stderr, "warning: curl_multi_cleanup failed\n");
    }
    if (CURLSHE_OK != curl_share_cleanup(ccs->curlsh)) {
        fprintf(stderr, "warning: curl_share_cleanup failed\n");
    }
    return NULL;
}

struct JSHttpClientImplementation *
js_curl_http_client_create()
{
    struct JSHttpClientImplementation *impl = NULL;
    struct CurlClientState *ccs = NULL;
    int res;

    ccs = malloc(sizeof *ccs);
    if (!ccs) {
        goto error;
    }

    pthread_mutex_init(&ccs->mutex, NULL);
    ccs->started = FALSE;
    ccs->stopped = FALSE;
    ccs->last_request_id = 0;
    ccs->curlsh = curl_share_init();
    if (!ccs->curlsh) {
      goto error;
    }
    ccs->curlm = curl_multi_init();
    if (!ccs->curlm) {
      goto error;
    }
    init_list_head(&ccs->request_list);
    init_list_head(&ccs->add_queue);
    init_list_head(&ccs->cancel_queue);

    curl_share_setopt(ccs->curlsh, CURLSHOPT_SHARE, CURL_LOCK_DATA_DNS);
    curl_share_setopt(ccs->curlsh, CURLSHOPT_SHARE, CURL_LOCK_DATA_SSL_SESSION);
    curl_share_setopt(ccs->curlsh, CURLSHOPT_SHARE, CURL_LOCK_DATA_CONNECT);

    impl = malloc(sizeof *impl);
    if (!impl) {
        goto error;
    }
    impl->req_create = &create_impl;
    impl->req_cancel = &destroy_impl;
    impl->cls = ccs;

    res = pthread_create(&ccs->thread, NULL, &curl_multi_thread_run, ccs);
    ccs->started = TRUE;

    if (0 != res) {
        goto error;
    }

    return impl;
error:
    if (ccs) {
      curl_share_cleanup(ccs->curlsh);
      curl_multi_cleanup(ccs->curlm);
      free(ccs);
    }
    if (impl) {
      free(impl);
    }
    return NULL;
}

static void
destroy_client_state(struct CurlClientState *ccs)
{
    struct list_head *el, *el1;
    if (!ccs) {
        return;
    }
    if (ccs->started == TRUE) {
        void *retval;
        int res;

        pthread_mutex_lock(&ccs->mutex);
        ccs->stopped = TRUE;
        pthread_mutex_unlock(&ccs->mutex);
        curl_multi_wakeup(ccs->curlm);
        res = pthread_join(ccs->thread, &retval);
        if (0 != res) {
            fprintf(stderr, "warning: could not join with curl thread\n");
        }
        ccs->started = FALSE;
    }
    pthread_mutex_lock(&ccs->mutex);
    list_for_each_safe(el, el1, &ccs->request_list) {
        struct CurlRequestState *crs = list_entry(el, struct CurlRequestState, link_req);
        destroy_curl_request_state(crs);
    }
    pthread_mutex_unlock(&ccs->mutex);
    pthread_mutex_destroy(&ccs->mutex);
    free(ccs);
}

void
js_curl_http_client_destroy(struct JSHttpClientImplementation *impl)
{
    if (!impl) {
        return;
    }
    destroy_client_state(impl->cls);
    impl->cls = NULL;
    free(impl);
}

