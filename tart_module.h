#ifndef _TALERCRYPTO_H
#define _TALERCRYPTO_H

#include "quickjs/quickjs-libc.h"

JSModuleDef *tart_init_module_talercrypto(JSContext *ctx, const char *module_name);

#endif /* _TALERCRYPTO_H */
