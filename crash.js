import * as std from "std";
import * as os from "os";

function openPromise() {
  let resolve = null;
  let reject = null;
  const promise = new Promise((res, rej) => {
    resolve = res;
    reject = rej;
  });
  if (!(resolve && reject)) {
    throw Error();
  }
  return { resolve, reject, promise };
}

let listener;

async function waitUntilDone(ws) {
  let p2;
  listener = (yn) => {
    if (!p2) {
      return;
    }
    if (yn)
      p2.resolve();
  };
  while (1) {
    p2 = openPromise();
    await p2.promise;
  }
}

waitUntilDone();

//os.setTimeout(() => {
//  std.gc();
//  listener(true);
//}, 0);

os.setTimeout(() => {
  listener(false);
  std.gc();
  std.gc();
}, 0);
