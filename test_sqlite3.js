import * as tart from "tart";
import * as std from "std";

function expectThrows(f) {
  try {
    f();
  } catch (e) {
    return e;
  }
  throw Error("expected exception, but function did not throw");
}

function expectStrictEquals(actual, expected) {
  if (actual !== expected) {
    throw Error();
  }
}

const db = tart.sqlite3Open(":memory:");

tart.sqlite3Exec(db, "create table foo ( name string unique, age number)");

let res;

const stmt1 = tart.sqlite3Prepare(db, "insert into foo(name, age) values ($name, $value)")
res = tart.sqlite3StmtRun(stmt1, { name: "foo", value: 42 });
console.log("stmt1 res:", res.lastInsertRowid);
res = tart.sqlite3StmtRun(stmt1, { name: "bar", value: 10 });
console.log("stmt1 res:", res.lastInsertRowid);

const stmt2 = tart.sqlite3Prepare(db, "select * from foo")
res = tart.sqlite3StmtGetAll(stmt2);

console.log("result:", JSON.stringify(res));

const stmt3 = tart.sqlite3Prepare(db, "select * from foo")
res = tart.sqlite3StmtGetFirst(stmt3);

console.log("result:", JSON.stringify(res));

tart.sqlite3Exec(db, "create table bla ( name string unique, data blob)");
const stmt4 = tart.sqlite3Prepare(db, "insert into bla(name, data) values ($name, $value)")
const d = new Uint8Array(4);
d[0] = 42;
d[1] = 43;
d[2] = 44;
d[3] = 46;
res = tart.sqlite3StmtRun(stmt4, { name: "v1", value: d });

tart.sqlite3Exec(db, "create table t ( name string unique, data blob)");
const stmt5 = tart.sqlite3Prepare(db, "insert into t(name, data) values ($name, $value)")

res = tart.sqlite3StmtRun(stmt4, { name: "n1", value: "v1" });
res = tart.sqlite3StmtRun(stmt4, { name: "n2", value: "v2" });

const exc = expectThrows(() => {
  res = tart.sqlite3StmtRun(stmt4, { name: "n2", value: "v3" });
});

expectStrictEquals(exc.code, "SQLITE_CONSTRAINT");

// Make sure that we can continue properly after a failed run
res = tart.sqlite3StmtRun(stmt4, { name: "n4", value: "v4" });
