/*
   This file is part of GNU Taler
   Copyright (C) 2022 Taler Systems SA

   GNU Taler is free software; you can redistribute it and/or modify it under the
   terms of the GNU Affero General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License along with
   GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

#include "quickjs/cutils.h"
#include "quickjs/list.h"
#include "quickjs/quickjs-libc.h"

#include <sodium.h>
#include <mbedtls/hkdf.h>
#include <mbedtls/bignum.h>
#include <mbedtls/error.h>

#include <ctype.h>
#include <string.h>
#include <assert.h>

#include <arpa/inet.h>

#if defined(__APPLE__)
#include <sqlite3.h>
#else
#include "sqlite3/sqlite3.h"
#endif

static JSValue js_encode_utf8(JSContext *ctx, JSValueConst this_val,
                              int argc, JSValueConst *argv)
{
    const char *str;
    size_t len;
    JSValue buf;
    str = JS_ToCStringLen2(ctx, &len, argv[0], FALSE);
    // FIXME: Don't copy buffer but pass destructor function
    buf = JS_NewArrayBufferCopy(ctx, (const uint8_t*) str, len);
    JS_FreeCString(ctx, str);
    return buf;
}

static JSValue js_random_bytes(JSContext *ctx, JSValueConst this_val,
                               int argc, JSValueConst *argv)
{
    uint32_t nbytes;
    JSValue buf;
    if (0 != JS_ToUint32(ctx, &nbytes, argv[0])) {
      return JS_EXCEPTION;
    }
    {
      uint8_t randbuf[nbytes];
      randombytes_buf (randbuf, nbytes);
      buf = JS_NewArrayBufferCopy(ctx, randbuf, nbytes);
    }
    return buf;
}

/**
 * Get the decoded value corresponding to a character according to Crockford
 * Base32 encoding.
 *
 * @param a a character
 * @return corresponding numeric value
 */
static unsigned int
getValue__ (unsigned char a)
{
  unsigned int dec;

  switch (a)
  {
  case 'O':
  case 'o':
    a = '0';
    break;

  case 'i':
  case 'I':
  case 'l':
  case 'L':
    a = '1';
    break;

  /* also consider U to be V */
  case 'u':
  case 'U':
    a = 'V';
    break;

  default:
    break;
  }
  if ((a >= '0') && (a <= '9'))
    return a - '0';
  if ((a >= 'a') && (a <= 'z'))
    a = toupper (a);
  /* return (a - 'a' + 10); */
  dec = 0;
  if ((a >= 'A') && (a <= 'Z'))
  {
    if ('I' < a)
      dec++;
    if ('L' < a)
      dec++;
    if ('O' < a)
      dec++;
    if ('U' < a)
      dec++;
    return(a - 'A' + 10 - dec);
  }
  return -1;
}

static JSValue js_talercrypto_encode_crock(JSContext *ctx, JSValue this_val,
                                           int argc, JSValueConst *argv)
{
  size_t size;
  uint8_t *buf;
  uint8_t *out = NULL;
  size_t out_size;
  // 32 characters for encoding
  static char *encTable__ = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";
  unsigned int wpos;
  unsigned int rpos;
  unsigned int bits;
  unsigned int vbit;
  const unsigned char *udata;
  JSValue ret_val;

  ret_val = JS_UNDEFINED;

  buf = JS_GetArrayBuffer(ctx, &size, argv[0]);

  if (!buf) {
    goto exception;
  }

  assert (size < SIZE_MAX / 8 - 4);
  out_size = size * 8;

  if (out_size % 5 > 0)
    out_size += 5 - out_size % 5;
  out_size /= 5;

  out = malloc (out_size + 1);
  memset (out, 0, out_size + 1);
  if (!out) {
    goto exception;
  }

  udata = buf;
  if (out_size < (size * 8 + 4) / 5) {
    goto exception;
  }
  vbit = 0;
  wpos = 0;
  rpos = 0;
  bits = 0;
  while ((rpos < size) || (vbit > 0))
  {
    if ((rpos < size) && (vbit < 5))
    {
      bits = (bits << 8) | udata[rpos++];     /* eat 8 more bits */
      vbit += 8;
    }
    if (vbit < 5)
    {
      bits <<= (5 - vbit);     /* zero-padding */
      assert (vbit == ((size * 8) % 5));
      vbit = 5;
    }
    if (wpos >= out_size)
    {
      goto exception;
    }
    out[wpos++] = encTable__[(bits >> (vbit - 5)) & 31];
    vbit -= 5;
  }
  assert (0 == vbit);
  if (wpos < out_size)
    out[wpos] = '\0';

  ret_val = JS_NewString(ctx, (char *) out);

done:
  if (NULL != out) {
    free(out);
  }
  return ret_val;
exception:
  ret_val = JS_EXCEPTION;
  goto done;
}

static JSValue js_talercrypto_decode_crock(JSContext *ctx, JSValue this_val,
                                           int argc, JSValueConst *argv)
{
  size_t rpos;
  size_t wpos;
  unsigned int bits;
  unsigned int vbit;
  int ret;
  int shift;
  size_t enclen;
  size_t encoded_len;
  const char *enc;
  JSValue ret_val = JS_UNDEFINED;
  unsigned char *uout = NULL;
  size_t out_size;
  JSValue abuf;

  enc = JS_ToCStringLen2(ctx, &enclen, argv[0], FALSE);
  if (!enc) {
    goto exception;
  }

  out_size = (enclen * 5) / 8;
  encoded_len = out_size * 8;
  uout = malloc(out_size);
  assert (out_size < SIZE_MAX / 8);
  wpos = out_size;
  rpos = enclen;
  if ((encoded_len % 5) > 0)
  {
    vbit = encoded_len % 5;   /* padding! */
    shift = 5 - vbit;
    bits = (ret = getValue__ (enc[--rpos])) >> shift;
  }
  else
  {
    vbit = 5;
    shift = 0;
    bits = (ret = getValue__ (enc[--rpos]));
  }
  if ((encoded_len + shift) / 5 != enclen) {
    JS_ThrowTypeError(ctx, "wrong encoded length");
    goto exception;
  }

  if (-1 == ret) {
    JS_ThrowTypeError(ctx, "invalid character in encoding");
    goto exception;
  }
  while (wpos > 0)
  {
    if (0 == rpos)
    {
      goto exception;
    }
    bits = ((ret = getValue__ (enc[--rpos])) << vbit) | bits;
    if (-1 == ret) {
      goto exception;
    }
    vbit += 5;
    if (vbit >= 8)
    {
      uout[--wpos] = (unsigned char) bits;
      bits >>= 8;
      vbit -= 8;
    }
  }
  if ((0 != rpos) || (0 != vbit)) {
    JS_ThrowTypeError(ctx, "rpos or vbit not zero");
    goto exception;
  }
  abuf = JS_NewArrayBufferCopy(ctx, uout, out_size);
  if (JS_IsException(abuf)) {
    goto exception;
  }
  ret_val = JS_NewTypedArray(ctx, abuf, 1);
done:
  JS_FreeCString(ctx, enc);
  if (uout) {
    free(uout);
  }
  return ret_val;
exception:
  ret_val = JS_EXCEPTION;
  goto done;
}

uint8_t *expect_fixed_buffer(JSContext *ctx,
                             JSValue val, size_t len,
                             const char *msg)
{
    uint8_t *buf;
    size_t sz;

    buf = JS_GetArrayBuffer(ctx, &sz, val);
    if (!buf) {
        return NULL;
    }
    if (sz != len) {
        JS_ThrowTypeError(ctx, "invalid length for %s", msg);
        return NULL;
    }
    return buf;
}

int
expect_mpi(JSContext *ctx,
           JSValue val,
           const char *msg,
           mbedtls_mpi *ret_mpi)
{
    uint8_t *buf;
    size_t sz;

    buf = JS_GetArrayBuffer(ctx, &sz, val);
    if (!buf) {
        return -1;
    }
    if (0 != mbedtls_mpi_read_binary(ret_mpi, buf, sz)) {
        return -1;
    }
    return 0;
}

#define CHECK(x) do { if (!(x)) { abort(); } } while (0)

typedef struct {
  mbedtls_mpi N;
  mbedtls_mpi e;
} RsaPub;

typedef uint8_t BlindingKeySecret[32];
typedef uint8_t HashCode[64];

int
rsa_public_key_decode(RsaPub *pkey, uint8_t *inbuf, size_t inbuf_len)
{
    size_t sz;
    uint8_t *p; /* read pointer */
    size_t mod_len;
    size_t exp_len;
    int ret;

    CHECK(NULL != pkey);
    if (inbuf_len < 4) {
        ret = -1;
        goto cleanup;
    }
    p = inbuf;
    mod_len = ntohs(*((uint16_t *) p));
    p += sizeof(uint16_t);
    exp_len = ntohs(*((uint16_t *) p));
    sz = 4 + mod_len + exp_len;
    if (sz != inbuf_len) {
        ret = -1;
        goto cleanup;
    }
    p += sizeof(uint16_t);
    MBEDTLS_MPI_CHK(mbedtls_mpi_read_binary(&pkey->N, p, mod_len));
    p += mod_len;
    MBEDTLS_MPI_CHK(mbedtls_mpi_read_binary(&pkey->e, p, exp_len));

cleanup:
    if (ret != 0) {
        mbedtls_mpi_free(&pkey->N);
        mbedtls_mpi_free(&pkey->e);
    }
    return ret;
}

int
expect_rsa_pub(JSContext *ctx,
           JSValue val,
           const char *msg,
           RsaPub *ret_rsa_pub)
{
    uint8_t *rsa_enc;
    size_t rsa_enc_len;
    int ret = -1;

    rsa_enc = JS_GetArrayBuffer(ctx, &rsa_enc_len, val);
    if (!rsa_enc) {
        goto cleanup;
    }
    if (0 != rsa_public_key_decode(ret_rsa_pub, rsa_enc, rsa_enc_len)) {
        JS_ThrowTypeError(ctx, "rsa pubkey");
        goto cleanup;
    }
    ret = 0;
cleanup:
    return ret;
}

#define REQUIRE(cond, _label) do { if (!(cond)) { goto _label; } } while (0)


static JSValue make_js_ta_copy(JSContext *ctx, uint8_t *data, size_t size)
{
  JSValue array_buf;

  array_buf = JS_NewArrayBufferCopy(ctx, data, size);
  if (JS_IsException(array_buf)) {
    return JS_EXCEPTION;
  }
  return JS_NewTypedArray(ctx, array_buf, 1);
}

/**
 * Make a JS typed array from an mbedtls MPI.
*/
static JSValue make_js_ta_mpi(JSContext *ctx, const mbedtls_mpi *v)
{
    JSValue array_buf;
    size_t sz;
    uint8_t *buf = NULL;
    JSValue ret_val;

    sz = mbedtls_mpi_size(v);
    buf = malloc(sz);
    if (!buf) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }

    if (0 != mbedtls_mpi_write_binary(v, buf, sz)) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }

    // FIXME(#perf): Don't copy
    array_buf = JS_NewArrayBufferCopy(ctx, buf, sz);
    if (JS_IsException(array_buf)) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }
    ret_val = JS_NewTypedArray(ctx, array_buf, 1);
cleanup:
    if (buf) {
        free(buf);
    }
    return ret_val;
}

static JSValue js_talercrypto_hash(JSContext *ctx, JSValue this_val,
                                   int argc, JSValueConst *argv)
{
  size_t size;
  uint8_t *buf;
  unsigned char h[crypto_hash_BYTES];

  buf = JS_GetArrayBuffer(ctx, &size, argv[0]);
  if (!buf) {
    return JS_EXCEPTION;
  }
  crypto_hash_sha512(h, buf, size);

  return make_js_ta_copy(ctx, h, crypto_hash_BYTES);
}

static JSValue js_talercrypto_hash_argon2id(JSContext *ctx, JSValue this_val,
                                            int argc, JSValueConst *argv)
{
    size_t pw_len;
    size_t salt_len;
    uint32_t iters;
    uint32_t mem_size;
    uint32_t hash_len;
    uint8_t *pw;
    uint8_t *salt;
    uint8_t *hash = NULL;

    JSValue ret_val;

    // password: Uint8Array
    pw = JS_GetArrayBuffer(ctx, &pw_len, argv[0]);
    if (!pw) {
        goto exception;
    }

    // salt: Uint8Array
    salt = JS_GetArrayBuffer(ctx, &salt_len, argv[1]);
    if (!salt) {
        goto exception;
    }
    if (salt_len != crypto_pwhash_SALTBYTES) {
        JS_ThrowTypeError(ctx, "invalid salt size");
        goto exception;
    }

    // iterations: number
    if (0 != JS_ToUint32(ctx, &iters, argv[2])) {
        goto exception;
    }

    // memorySize: number (kibibytes)
    if (0 != JS_ToUint32(ctx, &mem_size, argv[3])) {
        goto exception;
    }

    // hashLength: number
    if (0 != JS_ToUint32(ctx, &hash_len, argv[4])) {
        goto exception;
    }

    hash = malloc(hash_len);

    if (crypto_pwhash(hash,
                      hash_len,
                      (const char*) pw,
                      pw_len,
                      salt,
                      iters,
                      mem_size * 1024,
                      crypto_pwhash_ALG_ARGON2ID13) != 0) {
        JS_ThrowInternalError(ctx, "crypto_pwhash() call failed");
        goto exception;
    }
    ret_val = make_js_ta_copy(ctx, hash, hash_len);
 done:
    if (NULL != hash) {
        free(hash);
    }
    return ret_val;
 exception:
    ret_val = JS_EXCEPTION;
    goto done;
}

static JSValue js_talercrypto_eddsa_key_get_public(JSContext *ctx, JSValue this_val,
                                                   int argc, JSValueConst *argv)
{
  uint8_t *buf;
  unsigned char pk[crypto_sign_PUBLICKEYBYTES];
  unsigned char sk[crypto_sign_SECRETKEYBYTES];

  buf = expect_fixed_buffer(ctx, argv[0], 32, "eddsa private key");
  
  if (!buf) {
    return JS_EXCEPTION;
  }

  crypto_sign_seed_keypair(pk, sk, buf);
  // FIXME: clean up stack!

  return make_js_ta_copy(ctx, pk, crypto_sign_PUBLICKEYBYTES);
}

static JSValue js_talercrypto_ecdhe_key_get_public(JSContext *ctx, JSValue this_val,
                                                   int argc, JSValueConst *argv)
{
  uint8_t *buf;
  unsigned char pk[crypto_scalarmult_BYTES];

  buf = expect_fixed_buffer(ctx, argv[0], 32, "ecdh private key");
  
  if (!buf) {
    return JS_EXCEPTION;
  }

  if (0 != crypto_scalarmult_base(pk, buf)) {
    return JS_EXCEPTION;
  }
  // FIXME: clean up stack!

  return make_js_ta_copy(ctx, pk, crypto_sign_PUBLICKEYBYTES);
}

/**
 * (msg, priv) => sig
 */
static JSValue js_talercrypto_eddsa_sign(JSContext *ctx, JSValue this_val,
                                         int argc, JSValueConst *argv)
{
    unsigned char *seed;
    size_t seed_size;
    unsigned char *data;
    size_t data_size;
    unsigned char sk[crypto_sign_SECRETKEYBYTES];
    unsigned char pk[crypto_sign_PUBLICKEYBYTES];
    unsigned char sig[64];
    int res;

    data = JS_GetArrayBuffer(ctx, &data_size, argv[0]);
    if (!data) {
        return JS_EXCEPTION;
    }

    seed = JS_GetArrayBuffer(ctx, &seed_size, argv[1]);
    if (!seed) {
        return JS_EXCEPTION;
    }
    if (seed_size != 32) {
        return JS_ThrowTypeError(ctx, "invalid private key size");
    }

    if (0 != crypto_sign_seed_keypair(pk, sk, seed)) {
        return JS_EXCEPTION;
    }

    res = crypto_sign_detached((uint8_t *)sig,
                               NULL,
                               (uint8_t *)data,
                               data_size,
                               sk);
    if (res != 0) {
        return JS_EXCEPTION;
    }
    return make_js_ta_copy(ctx, sig, 64);
}

/**
 * (msg, sig, pub) -> bool
 */
static JSValue js_talercrypto_eddsa_verify(JSContext *ctx, JSValue this_val,
                                           int argc, JSValueConst *argv)
{
  unsigned char *msg;
  size_t msg_size;
  unsigned char *sig;
  size_t sig_size;
  unsigned char *pub;
  size_t pub_size;
  int res;

  msg = JS_GetArrayBuffer(ctx, &msg_size, argv[0]);
  if (!msg) {
    return JS_EXCEPTION;
  }
  sig = JS_GetArrayBuffer(ctx, &sig_size, argv[1]);
  if (!sig) {
    return JS_EXCEPTION;
  }
  if (sig_size != 64) {
    return JS_ThrowTypeError(ctx, "invalid signature size");
  }
  pub = JS_GetArrayBuffer(ctx, &pub_size, argv[2]);
  if (!pub) {
    return JS_EXCEPTION;
  }
  if (pub_size != 32) {
    return JS_ThrowTypeError(ctx, "invalid public key size");
  }

  res = crypto_sign_verify_detached (sig, msg, msg_size, pub);
  return (res == 0) ? JS_TRUE : JS_FALSE;
}

/**
 * Returns 0 on success.
 */
static int
kdf(void *okm, size_t okm_len,
    const void *ikm, size_t ikm_len,
    const void *salt, size_t salt_len,
    const void *info, size_t info_len)
{
    const mbedtls_md_info_t *md_extract;
    const mbedtls_md_info_t *md_expand;
    int ret = MBEDTLS_ERR_ERROR_CORRUPTION_DETECTED;
    unsigned char prk[MBEDTLS_MD_MAX_SIZE];

    md_extract = mbedtls_md_info_from_type(MBEDTLS_MD_SHA512);
    md_expand = mbedtls_md_info_from_type(MBEDTLS_MD_SHA256);
    if (NULL == md_extract) {
        return -1;
    }
    if (NULL == md_expand) {
        return -1;
    }

    ret = mbedtls_hkdf_extract(md_extract, salt, salt_len, ikm, ikm_len, prk);

    if (ret != 0) {
        return ret;
    }

    ret = mbedtls_hkdf_expand(md_expand, prk, mbedtls_md_get_size(md_extract),
                              info, info_len, okm, okm_len);
    return ret;
}

/**
 * (outLen, ikm, salt?, info?) -> output
 */
static JSValue js_talercrypto_kdf(JSContext *ctx, JSValue this_val,
                                  int argc, JSValueConst *argv)
{
    size_t salt_len;
    size_t ikm_len;
    size_t info_len;
    size_t okm_len;
    uint8_t *salt;
    uint8_t *ikm;
    uint8_t *info;
    uint8_t *okm = NULL;
    uint32_t out_bytes;
    JSValue ret_val;
    int ret;

    if (0 != JS_ToUint32(ctx, &out_bytes, argv[0])) {
        goto exception;
    }
    okm_len = out_bytes;

    ikm = JS_GetArrayBuffer(ctx, &ikm_len, argv[1]);
    if (!ikm) {
        goto exception;
    }

    if (JS_IsUndefined(argv[2])) {
        salt = NULL;
        salt_len = 0;
    } else {
        salt = JS_GetArrayBuffer(ctx, &salt_len, argv[2]);
        if (!salt) {
            goto exception;
        }
    }

    if (JS_IsUndefined(argv[3])) {
        info = NULL;
        info_len = 0;
    } else {
        info = JS_GetArrayBuffer(ctx, &info_len, argv[3]);
        if (!info) {
            goto exception;
        }
    }

    okm = malloc(okm_len);

    ret = kdf(okm, okm_len, ikm, ikm_len, salt, salt_len, info, info_len);

    if (ret != 0) {
        JS_ThrowInternalError(ctx, "kdf() call failed");
        goto exception;
    }
    ret_val = make_js_ta_copy(ctx, okm, okm_len);
done:
    if (NULL != okm) {
        free(okm);
    }
    return ret_val;
exception:
    ret_val = JS_EXCEPTION;
    goto done;
}

/**
 * (ecdhePriv, eddsaPub) -> keyMaterial
 */
static JSValue js_talercrypto_kx_ecdh_eddsa(JSContext *ctx, JSValue this_val,
                                            int argc, JSValueConst *argv)
{
  JSValue ret_val;
  uint8_t p[crypto_scalarmult_BYTES];
  uint8_t *ecdh_priv;
  uint8_t *eddsa_pub;
  uint8_t curve25510_pk[crypto_scalarmult_BYTES];
  uint8_t key_material[crypto_hash_BYTES];

  ecdh_priv = expect_fixed_buffer(ctx, argv[0], 32, "ecdhe priv");
  REQUIRE(ecdh_priv, exception);

  eddsa_pub = expect_fixed_buffer(ctx, argv[1], 32, "eddsa pub");
  REQUIRE(eddsa_pub, exception);

  if (0 != crypto_sign_ed25519_pk_to_curve25519(curve25510_pk, eddsa_pub)) {
    goto exception;
  }
  if (0 != crypto_scalarmult(p, ecdh_priv, curve25510_pk)) {
    goto exception;
  }
  if (0 != crypto_hash(key_material, p, 32)) {
    JS_ThrowTypeError(ctx, "hashing failed");
    goto exception;
  }
  ret_val = make_js_ta_copy(ctx, key_material, crypto_hash_BYTES);
done:
  return ret_val;
exception:
  ret_val = JS_EXCEPTION;
  goto done;
}

/**
 * (eddsaPriv, ecdhePub) -> keyMaterial
 */
static JSValue js_talercrypto_kx_eddsa_ecdh(JSContext *ctx, JSValue this_val,
                                            int argc, JSValueConst *argv)
{
  JSValue ret_val;
  uint8_t *priv;
  uint8_t *pub;
  uint8_t hc[crypto_hash_BYTES];
  uint8_t a[crypto_scalarmult_SCALARBYTES];
  uint8_t p[crypto_scalarmult_BYTES];
  uint8_t key_material[crypto_hash_BYTES];

  priv = expect_fixed_buffer(ctx, argv[0], 32, "eddsa priv");
  REQUIRE(priv, exception);
  pub = expect_fixed_buffer(ctx, argv[1], 32, "ecdh pub");
  REQUIRE(pub, exception);

  crypto_hash(hc, priv, 32);
  memcpy (a, &hc, 32);
  if (0 != crypto_scalarmult(p, a, pub)) {
    goto exception;
  }
  crypto_hash(key_material, p, crypto_scalarmult_BYTES);
  ret_val = make_js_ta_copy(ctx, key_material, crypto_hash_BYTES);
done:
  return ret_val;
exception:
  ret_val = JS_EXCEPTION;
  goto done;
}

/** FIXME: Should return int */
void
kdf_mod_mpi(mbedtls_mpi *r,
            const mbedtls_mpi *n,
            const void *xts, size_t xts_len,
            const void *skm, size_t skm_len,
            const char *ctx)
{
  int rc;
  size_t nbits;
  uint16_t ctr;
  size_t ctxlen = strlen(ctx);
  size_t my_ctx_len = ctxlen + 2;
  unsigned char *my_ctx = malloc(my_ctx_len);
  uint16_t *ctr_nbo_p = (uint16_t *) (my_ctx + ctxlen);

  memcpy(my_ctx, ctx, ctxlen);

  nbits = mbedtls_mpi_bitlen(n);
  ctr = 0;
  while (1) {
    /* Not clear if n is always divisible by 8 */
    size_t bsize = (nbits - 1) / 8 + 1;
    uint8_t buf[bsize];

    *ctr_nbo_p = htons (ctr);

    rc = kdf (buf, bsize,
              skm, skm_len,
              xts, xts_len,
              my_ctx, my_ctx_len);
    CHECK(0 == rc);
    rc = mbedtls_mpi_read_binary(r, buf, bsize);
    CHECK(0 == rc);
    while (1) {
      size_t rlen = mbedtls_mpi_bitlen(r);
      if (rlen <= nbits) {
        break;
      }
      mbedtls_mpi_set_bit(r, rlen - 1, 0);
    }
    ++ctr;
    /* We reject this FDH if either r > n and retry with another ctr */
    if (0 > mbedtls_mpi_cmp_mpi (r, n)) {
      break;
    }
    mbedtls_mpi_free (r);
  }
  free(my_ctx);
}


/**
 * Test for malicious RSA key.
 *
 * Assuming n is an RSA modulous and r is generated using a call to
 * GNUNET_CRYPTO_kdf_mod_mpi, if gcd(r,n) != 1 then n must be a
 * malicious RSA key designed to deanomize the user.
 *
 * @param r KDF result
 * @param n RSA modulus
 * @return 0 if gcd(r,n) = 1, 1 means RSA key is malicious, negative is syserror
 */
static int
rsa_gcd_validate (mbedtls_mpi *r,
                  const mbedtls_mpi *n)
{
  mbedtls_mpi g;
  int ret;

  mbedtls_mpi_init(&g);
  MBEDTLS_MPI_CHK(mbedtls_mpi_gcd(&g, r, n));

  if (mbedtls_mpi_cmp_int(&g, 1) == 0) {
    ret = 0;
  } else {
    goto cleanup;
  }

cleanup:
  mbedtls_mpi_free(&g);
  return ret;
}


int
rsa_blinding_key_derive(mbedtls_mpi *r,
                        const RsaPub *pkey,
                        const BlindingKeySecret *bks)
{
    /* Trusts bks' randomness more */
    const char *xts = "Blinding KDF extractor HMAC key";

    kdf_mod_mpi(r,
                &pkey->N,
                xts, strlen(xts),
                bks, sizeof(*bks),
                "Blinding KDF");

    if (0 != rsa_gcd_validate (r, &pkey->N)) {
        return -1;
    }

    return 0;
}

int
rsa_public_key_encode(const RsaPub *pkey, uint8_t **outbuf, size_t *outbuf_len)
{
    size_t sz;
    uint8_t *buf;
    uint8_t *p; /* write pointer */
    size_t mod_len;
    size_t exp_len;
    int ret;

    *outbuf = NULL;
    *outbuf_len = 0;

    mod_len = mbedtls_mpi_size(&pkey->N);
    exp_len = mbedtls_mpi_size(&pkey->e);
    sz = 2 + 2 + exp_len + mod_len;
    buf = malloc(sz);
    if (!buf) {
        return -1;
    }

    p = buf;
    *((uint16_t *) p) = htons(mod_len);
    p += sizeof (uint16_t);
    *((uint16_t *) p) = htons(exp_len);
    p += sizeof (uint16_t);
    MBEDTLS_MPI_CHK(mbedtls_mpi_write_binary(&pkey->N, p, mod_len));
    p += mod_len;
    MBEDTLS_MPI_CHK(mbedtls_mpi_write_binary(&pkey->e, p, exp_len));

    *outbuf = buf;
    *outbuf_len = sz;

cleanup:
    if (0 != ret) {
        free(buf);
    }
    return ret;
}


void
rsa_public_key_init(RsaPub *pkey)
{
    CHECK(NULL != pkey);
    mbedtls_mpi_init(&pkey->e);
    mbedtls_mpi_init(&pkey->N);
}

void
rsa_public_key_free(RsaPub *pkey)
{
  if (!pkey) {
    return;
  }
  mbedtls_mpi_free(&pkey->e);
  mbedtls_mpi_free(&pkey->N);
}


int
rsa_full_domain_hash (mbedtls_mpi *r, const RsaPub *pkey,
                      const HashCode *hash)
{
  uint8_t *xts;
  size_t xts_len;

  /* We key with the public denomination key as a homage to RSA-PSS by
     Mihir Bellare and Phillip Rogaway.  Doing this lowers the degree
     of the hypothetical polyomial-time attack on RSA-KTI created by a
     polynomial-time one-more forgary attack.  Yey seeding! */
  rsa_public_key_encode(pkey, &xts, &xts_len);

  kdf_mod_mpi(r,
              &pkey->N,
              xts, xts_len,
              hash, sizeof(*hash),
              "RSA-FDA FTpsW!");
  free(xts);
  if (0 == rsa_gcd_validate (r, &pkey->N)) {
    return 0;
  }
  return 1;
}

int
rsa_blind(const HashCode *hash,
          const BlindingKeySecret *bks,
          const RsaPub *pkey,
          uint8_t **buf,
          size_t *buf_size)
{
  mbedtls_mpi bkey, data, r_e, data_r_e;
  size_t outsize;
  uint8_t *outbuf;
  int ret;

  CHECK(buf != NULL);
  CHECK(buf_size != NULL);

  *buf = NULL;
  *buf_size = 0;

  mbedtls_mpi_init(&bkey);
  mbedtls_mpi_init(&data);
  mbedtls_mpi_init(&r_e);
  mbedtls_mpi_init(&data_r_e);

  MBEDTLS_MPI_CHK(rsa_full_domain_hash (&data, pkey, hash));
  MBEDTLS_MPI_CHK(rsa_blinding_key_derive (&bkey, pkey, bks));
  MBEDTLS_MPI_CHK(mbedtls_mpi_exp_mod(&r_e, &bkey, &pkey->e, &pkey->N, NULL));
  MBEDTLS_MPI_CHK(mbedtls_mpi_mul_mpi(&data_r_e, &data, &r_e));
  MBEDTLS_MPI_CHK(mbedtls_mpi_mod_mpi(&data_r_e, &data_r_e, &pkey->N));

  outsize = (mbedtls_mpi_bitlen(&data_r_e) + 7) / 8;
  outbuf = malloc(outsize);

  MBEDTLS_MPI_CHK(mbedtls_mpi_write_binary(&data_r_e, outbuf, outsize));

  *buf = outbuf;
  *buf_size = outsize;
  ret = 0;

cleanup:
  mbedtls_mpi_free(&data);
  mbedtls_mpi_free(&bkey);
  mbedtls_mpi_free(&r_e);
  mbedtls_mpi_free(&data_r_e);
  return ret;
}

int
rsa_unblind (const mbedtls_mpi *sig_blinded,
             const BlindingKeySecret *bks,
             const RsaPub *pkey,
             mbedtls_mpi *sig_ret)
{
  mbedtls_mpi bkey, r_inv, ubsig;
  int ret;

  mbedtls_mpi_init(&bkey);
  mbedtls_mpi_init(&r_inv);
  mbedtls_mpi_init(&ubsig);

  MBEDTLS_MPI_CHK(rsa_blinding_key_derive (&bkey, pkey, bks));
  MBEDTLS_MPI_CHK(mbedtls_mpi_inv_mod(&r_inv, &bkey, &pkey->N));
  MBEDTLS_MPI_CHK(mbedtls_mpi_mul_mpi(&ubsig, sig_blinded, &r_inv));
  MBEDTLS_MPI_CHK(mbedtls_mpi_copy(sig_ret, &ubsig));

cleanup:
  mbedtls_mpi_free(&bkey);
  mbedtls_mpi_free(&r_inv);
  mbedtls_mpi_free(&ubsig);
  return ret;
}


int
rsa_verify(const HashCode *hash,
           const mbedtls_mpi *sig,
           const RsaPub *pkey)
{
    mbedtls_mpi r;
    mbedtls_mpi sig_2;
    int ret;

    mbedtls_mpi_init(&r);
    mbedtls_mpi_init(&sig_2);

    /* Can fail if RSA key is malicious since rsa_gcd_validate failed here.
     * It should have failed during GNUNET_CRYPTO_rsa_blind too though,
     * so the exchange is being malicious in an unfamilair way, maybe
     * just trying to crash us.  Arguably, we've only an internal error
     * though because we should've detected this in our previous call
     * to GNUNET_CRYPTO_rsa_unblind. *///
    MBEDTLS_MPI_CHK(rsa_full_domain_hash(&r, pkey, hash));

    MBEDTLS_MPI_CHK(mbedtls_mpi_exp_mod(&sig_2, sig, &pkey->e, &pkey->N, NULL));

    if (0 != mbedtls_mpi_cmp_mpi(sig, &sig_2)) {
        ret = -1;
    } else {
        ret = 0;
    }

cleanup:
    mbedtls_mpi_free(&r);
    mbedtls_mpi_free(&sig_2);
    return ret;
}


/**
 * (hmsg, bks, rsaPub) -> blinded
 */
static JSValue js_talercrypto_rsa_blind(JSContext *ctx, JSValueConst this_val,
                                        int argc, JSValueConst *argv)
{
    HashCode *hmsg;
    BlindingKeySecret *bks;
    uint8_t *rsa_enc;
    size_t rsa_enc_len;
    RsaPub rsa_pub;
    JSValue ret_val = JS_UNDEFINED;
    uint8_t *out_buf = NULL;
    size_t out_len;

    rsa_public_key_init(&rsa_pub);

    hmsg = (HashCode *) expect_fixed_buffer(ctx, argv[0], 64, "hmsg");
    if (!hmsg) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }
    bks = (BlindingKeySecret *) expect_fixed_buffer(ctx, argv[1], 32, "bks");
    if (!bks) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }
    rsa_enc = JS_GetArrayBuffer(ctx, &rsa_enc_len, argv[2]);
    if (!rsa_enc) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }
    if (0 != rsa_public_key_decode(&rsa_pub, rsa_enc, rsa_enc_len)) {
        ret_val = JS_ThrowTypeError(ctx, "rsa pubkey");
        goto cleanup;
    }
    if (0 != rsa_blind(hmsg, bks, &rsa_pub, &out_buf, &out_len)) {
        ret_val = JS_ThrowInternalError(ctx, "blinding failed");
        goto cleanup;
    }

    ret_val = make_js_ta_copy(ctx, out_buf, out_len);
cleanup:
    if (NULL != out_buf) {
      free(out_buf);
      out_buf = NULL;
    }
    rsa_public_key_free(&rsa_pub);
    return ret_val;
}

/**
 * (blindSig, rsaPub, bks) -> ubsig
 */
static JSValue js_talercrypto_rsa_unblind(JSContext *ctx, JSValueConst this_val,
                                        int argc, JSValueConst *argv)
{
    JSValue ret_val = JS_UNDEFINED;
    mbedtls_mpi bsig;
    mbedtls_mpi sig_ret;
    RsaPub rsa_pub;
    BlindingKeySecret *bks;

    mbedtls_mpi_init(&bsig);
    mbedtls_mpi_init(&sig_ret);
    rsa_public_key_init(&rsa_pub);

    if (0 != expect_mpi(ctx, argv[0], "blindSig", &bsig)) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }
    if (0 != expect_rsa_pub(ctx, argv[1], "rsaPub", &rsa_pub)) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }
    bks = (BlindingKeySecret *) expect_fixed_buffer(ctx, argv[2], 32, "bks");
    if (!bks) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }

    if (0 != rsa_unblind(&bsig, bks, &rsa_pub, &sig_ret)) {
        ret_val = JS_ThrowInternalError(ctx, "unblinding failed");
        goto cleanup;
    }

    ret_val = make_js_ta_mpi(ctx, &sig_ret);

cleanup:
    mbedtls_mpi_free(&bsig);
    mbedtls_mpi_free(&sig_ret);
    rsa_public_key_free(&rsa_pub);
    return ret_val;
}

/**
 * (hm, rsaSig, rsaPub) -> bool
 */
static JSValue js_talercrypto_rsa_verify(JSContext *ctx, JSValueConst this_val,
                                        int argc, JSValueConst *argv)
{
    JSValue ret_val = JS_UNDEFINED;
    HashCode *hmsg;
    mbedtls_mpi sig;
    RsaPub rsa_pub;

    mbedtls_mpi_init(&sig);
    rsa_public_key_init(&rsa_pub);

    hmsg = (HashCode *) expect_fixed_buffer(ctx, argv[0], 64, "hmsg");
    if (!hmsg) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }

    if (0 != expect_mpi(ctx, argv[1], "sig", &sig)) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }

    if (0 != expect_rsa_pub(ctx, argv[2], "rsaPub", &rsa_pub)) {
        ret_val = JS_EXCEPTION;
        goto cleanup;
    }

    if (0 != rsa_verify(hmsg, &sig, &rsa_pub)) {
        ret_val = JS_FALSE;
        goto cleanup;
    }

    ret_val = JS_TRUE;

cleanup:
    mbedtls_mpi_free(&sig);
    rsa_public_key_free(&rsa_pub);
    return ret_val;

}

// (ArrayBuffer | TypedArray) => string
static JSValue js_decode_utf8(JSContext *ctx, JSValueConst this_val,
                              int argc, JSValueConst *argv)
{
    size_t psize;
    uint8_t *utf8_buf;
    JSValue ret_val;

    utf8_buf = JS_GetArrayBuffer(ctx, &psize, argv[0]);
    if (NULL == utf8_buf) {
        goto exception;
    }
    return JS_NewStringLen(ctx, (char *) utf8_buf, psize);
done:
    return ret_val;
exception:
    ret_val = JS_EXCEPTION;
    goto done;
}

static JSValue js_structured_clone(JSContext *ctx, JSValueConst this_val,
                                   int argc, JSValueConst *argv)
{
    uint8_t *buf;
    size_t len;
    JSValue obj;

    buf = JS_WriteObject(ctx, &len, argv[0], JS_WRITE_OBJ_REFERENCE);

    if (NULL == buf) {
      return JS_EXCEPTION;
    }

    obj = JS_ReadObject(ctx, buf, len, JS_WRITE_OBJ_REFERENCE);
    js_free(ctx, buf);
    return obj;
}

static JSClassID js_hash_state_class_id;

typedef struct {
  crypto_hash_sha512_state h;
  int finalized;
} TART_HashState;


static void js_hash_state_finalizer(JSRuntime *rt, JSValue val)
{
    TART_HashState *s = JS_GetOpaque(val, js_hash_state_class_id);
    /* Note: 's' can be NULL in case JS_SetOpaque() was not called */
    js_free_rt(rt, s);
}

static JSClassDef js_hash_state_class = {
    "HashState",
    .finalizer = js_hash_state_finalizer,
};

static JSValue js_talercrypto_hash_state_init(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
  TART_HashState *hstate;
  JSValue obj;

  hstate = js_malloc_rt(JS_GetRuntime(ctx), sizeof (TART_HashState));
  if (!hstate) {
    obj = JS_EXCEPTION;
    goto done;
  }
  hstate->finalized = FALSE;
  obj = JS_NewObjectClass(ctx, js_hash_state_class_id);
  crypto_hash_sha512_init(&hstate->h);
  JS_SetOpaque(obj, hstate);
  hstate = NULL;
  return obj;
done:
  if (NULL != hstate) {
    js_free_rt(JS_GetRuntime(ctx), hstate);
  }
  return obj;
}

static JSValue js_talercrypto_hash_state_update(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
  JSValue state = argv[0];
  JSValue data_val = argv[1];
  TART_HashState *hstate;
  uint8_t *data;
  size_t data_len;

  hstate = JS_GetOpaque(state, js_hash_state_class_id);

  if (!hstate) {
    return JS_ThrowTypeError(ctx, "expected HashState");
  }

  if (hstate->finalized) {
    return JS_ThrowTypeError(ctx, "already finalized");
  }

  data = JS_GetArrayBuffer(ctx, &data_len, data_val);

  if (0 != crypto_hash_sha512_update(&hstate->h, data, data_len)) {
    return JS_ThrowInternalError(ctx, "hashing failed");
  }

  return JS_UNDEFINED;
}

static JSValue js_talercrypto_hash_state_finish(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
    JSValue state = argv[0];
    TART_HashState *hstate;
    uint8_t hashval[crypto_hash_sha512_BYTES];

    hstate = JS_GetOpaque(state, js_hash_state_class_id);

    if (!hstate) {
        return JS_ThrowTypeError(ctx, "expected HashState");
    }

    if (hstate->finalized) {
        return JS_ThrowTypeError(ctx, "already finalized");
    }

    if (0 != crypto_hash_sha512_final(&hstate->h, hashval)) {
        return JS_ThrowInternalError(ctx, "hashing failed");
    }

    hstate->finalized = TRUE;

    return make_js_ta_copy(ctx, hashval, crypto_hash_sha512_BYTES);
}

static JSClassID js_sqlite3_database_class_id;
static JSClassID js_sqlite3_statement_class_id;

static void js_sqlite3_database_finalizer(JSRuntime *rt, JSValue val)
{
    sqlite3 *sqlite3_db;
    sqlite3_db = JS_GetOpaque(val, js_sqlite3_database_class_id);
    (void)sqlite3_close_v2(sqlite3_db);
    JS_SetOpaque(val, NULL);
}

static void js_sqlite3_statement_finalizer(JSRuntime *rt, JSValue val)
{
    sqlite3_stmt *stmt;

    stmt = JS_GetOpaque(val, js_sqlite3_statement_class_id);
    // FIXME: Check error code and warn?
    sqlite3_finalize(stmt);
    JS_SetOpaque(val, NULL);
}

static JSClassDef js_sqlite3_database_class = {
    .class_name = "Sqlite3Database",
    .finalizer = js_sqlite3_database_finalizer,
};

static JSClassDef js_sqlite3_statement_class = {
    .class_name = "Sqlite3Statement",
    .finalizer = js_sqlite3_statement_finalizer,
};

#define ERRCASE(c) case c: return #c

const char *translate_sqlite3_err_to_string(int errcode)
{
    switch (errcode) {
        ERRCASE(SQLITE_OK);
        ERRCASE(SQLITE_ERROR);
        ERRCASE(SQLITE_INTERNAL);
        ERRCASE(SQLITE_PERM);
        ERRCASE(SQLITE_ABORT);
        ERRCASE(SQLITE_BUSY);
        ERRCASE(SQLITE_LOCKED);
        ERRCASE(SQLITE_NOMEM);
        ERRCASE(SQLITE_READONLY);
        ERRCASE(SQLITE_INTERRUPT);
        ERRCASE(SQLITE_IOERR);
        ERRCASE(SQLITE_CORRUPT);
        ERRCASE(SQLITE_NOTFOUND);
        ERRCASE(SQLITE_FULL);
        ERRCASE(SQLITE_CANTOPEN);
        ERRCASE(SQLITE_PROTOCOL);
        ERRCASE(SQLITE_EMPTY);
        ERRCASE(SQLITE_SCHEMA);
        ERRCASE(SQLITE_TOOBIG);
        ERRCASE(SQLITE_CONSTRAINT);
        ERRCASE(SQLITE_MISMATCH);
        ERRCASE(SQLITE_MISUSE);
        ERRCASE(SQLITE_NOLFS);
        ERRCASE(SQLITE_AUTH);
        ERRCASE(SQLITE_FORMAT);
        ERRCASE(SQLITE_RANGE);
        ERRCASE(SQLITE_NOTADB);
        ERRCASE(SQLITE_NOTICE);
        ERRCASE(SQLITE_WARNING);
        ERRCASE(SQLITE_ROW);
        ERRCASE(SQLITE_DONE);
        ERRCASE(SQLITE_ERROR_MISSING_COLLSEQ);
        ERRCASE(SQLITE_ERROR_RETRY);
        ERRCASE(SQLITE_ERROR_SNAPSHOT);
        ERRCASE(SQLITE_IOERR_READ);
        ERRCASE(SQLITE_IOERR_SHORT_READ);
        ERRCASE(SQLITE_IOERR_WRITE);
        ERRCASE(SQLITE_IOERR_FSYNC);
        ERRCASE(SQLITE_IOERR_DIR_FSYNC);
        ERRCASE(SQLITE_IOERR_TRUNCATE);
        ERRCASE(SQLITE_IOERR_FSTAT);
        ERRCASE(SQLITE_IOERR_UNLOCK);
        ERRCASE(SQLITE_IOERR_RDLOCK);
        ERRCASE(SQLITE_IOERR_DELETE);
        ERRCASE(SQLITE_IOERR_BLOCKED);
        ERRCASE(SQLITE_IOERR_NOMEM);
        ERRCASE(SQLITE_IOERR_ACCESS);
        ERRCASE(SQLITE_IOERR_CHECKRESERVEDLOCK);
        ERRCASE(SQLITE_IOERR_LOCK);
        ERRCASE(SQLITE_IOERR_CLOSE);
        ERRCASE(SQLITE_IOERR_DIR_CLOSE);
        ERRCASE(SQLITE_IOERR_SHMOPEN);
        ERRCASE(SQLITE_IOERR_SHMSIZE);
        ERRCASE(SQLITE_IOERR_SHMLOCK);
        ERRCASE(SQLITE_IOERR_SHMMAP);
        ERRCASE(SQLITE_IOERR_SEEK);
        ERRCASE(SQLITE_IOERR_DELETE_NOENT);
        ERRCASE(SQLITE_IOERR_MMAP);
        ERRCASE(SQLITE_IOERR_GETTEMPPATH);
        ERRCASE(SQLITE_IOERR_CONVPATH);
        ERRCASE(SQLITE_IOERR_VNODE);
        ERRCASE(SQLITE_IOERR_AUTH);
        ERRCASE(SQLITE_IOERR_BEGIN_ATOMIC);
        ERRCASE(SQLITE_IOERR_COMMIT_ATOMIC);
        ERRCASE(SQLITE_IOERR_ROLLBACK_ATOMIC);
        ERRCASE(SQLITE_IOERR_DATA);
        ERRCASE(SQLITE_IOERR_CORRUPTFS);
        ERRCASE(SQLITE_LOCKED_SHAREDCACHE);
        ERRCASE(SQLITE_LOCKED_VTAB);
        ERRCASE(SQLITE_BUSY_RECOVERY);
        ERRCASE(SQLITE_BUSY_SNAPSHOT);
        ERRCASE(SQLITE_BUSY_TIMEOUT);
        ERRCASE(SQLITE_CANTOPEN_NOTEMPDIR);
        ERRCASE(SQLITE_CANTOPEN_ISDIR);
        ERRCASE(SQLITE_CANTOPEN_FULLPATH);
        ERRCASE(SQLITE_CANTOPEN_CONVPATH);
        ERRCASE(SQLITE_CANTOPEN_DIRTYWAL);
        ERRCASE(SQLITE_CANTOPEN_SYMLINK);
        ERRCASE(SQLITE_CORRUPT_VTAB);
        ERRCASE(SQLITE_CORRUPT_SEQUENCE);
        ERRCASE(SQLITE_CORRUPT_INDEX);
        ERRCASE(SQLITE_READONLY_RECOVERY);
        ERRCASE(SQLITE_READONLY_CANTLOCK);
        ERRCASE(SQLITE_READONLY_ROLLBACK);
        ERRCASE(SQLITE_READONLY_DBMOVED);
        ERRCASE(SQLITE_READONLY_CANTINIT);
        ERRCASE(SQLITE_READONLY_DIRECTORY);
        ERRCASE(SQLITE_ABORT_ROLLBACK);
        ERRCASE(SQLITE_CONSTRAINT_CHECK);
        ERRCASE(SQLITE_CONSTRAINT_COMMITHOOK);
        ERRCASE(SQLITE_CONSTRAINT_FOREIGNKEY);
        ERRCASE(SQLITE_CONSTRAINT_FUNCTION);
        ERRCASE(SQLITE_CONSTRAINT_NOTNULL);
        ERRCASE(SQLITE_CONSTRAINT_PRIMARYKEY);
        ERRCASE(SQLITE_CONSTRAINT_TRIGGER);
        ERRCASE(SQLITE_CONSTRAINT_UNIQUE);
        ERRCASE(SQLITE_CONSTRAINT_VTAB);
        ERRCASE(SQLITE_CONSTRAINT_ROWID);
        ERRCASE(SQLITE_CONSTRAINT_PINNED);
        ERRCASE(SQLITE_CONSTRAINT_DATATYPE);
        ERRCASE(SQLITE_NOTICE_RECOVER_WAL);
        ERRCASE(SQLITE_NOTICE_RECOVER_ROLLBACK);
#ifdef SQLITE_NOTICE_RBU
        ERRCASE(SQLITE_NOTICE_RBU);
#endif
        ERRCASE(SQLITE_WARNING_AUTOINDEX);
        ERRCASE(SQLITE_AUTH_USER);
        ERRCASE(SQLITE_OK_LOAD_PERMANENTLY);
        ERRCASE(SQLITE_OK_SYMLINK);
        default:
            return "SQLITE_UNKNOWN_ERRCODE";
    }
}


static JSValue throw_sqlite3_error(JSContext *ctx, sqlite3 *db)
{
    JSValue obj;

    obj = JS_NewError(ctx);
    if (JS_IsException(obj)) {
        /* out of memory: throw JS_NULL to avoid recursing */
        obj = JS_NULL;
        goto done;
    }

    JS_DefinePropertyValueStr(
        ctx,
        obj,
        "message",
        JS_NewString(ctx, sqlite3_errmsg(db)),
        JS_PROP_WRITABLE | JS_PROP_CONFIGURABLE);

    JS_DefinePropertyValueStr(
        ctx,
        obj,
        "code",
        JS_NewString(ctx, translate_sqlite3_err_to_string(sqlite3_errcode(db))),
        JS_PROP_WRITABLE | JS_PROP_CONFIGURABLE);

done:
    return JS_Throw(ctx, obj);
}


#define MAX_SAFE_INTEGER (((int64_t)1 << 53) - 1)
#define MIN_SAFE_INTEGER (-(((int64_t)1 << 53) - 1))

// (path: string, options?: { readonly?: boolean = false }) => Sqlite3Database
static JSValue js_sqlite3_open(JSContext *ctx, JSValue this_val,
                               int argc, JSValueConst *argv)
{
    JSValue ret_val = JS_UNINITIALIZED;
    JSValue db_obj = JS_UNINITIALIZED;
    const char *filename = NULL;
    sqlite3 *sqlite3_db = NULL;
    int ret;

    if (!JS_IsString(argv[0])) {
        ret_val = JS_ThrowTypeError(ctx, "filename argument required");
        goto done;
    }

    filename = JS_ToCString(ctx, argv[0]);

    if (!filename) {
        ret_val = JS_ThrowTypeError(ctx, "filename argument required");
        goto done;
    }

    fprintf(stderr, "opening sqlite3 db at %s\n", filename),
    ret = sqlite3_open_v2(filename, &sqlite3_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
    if (SQLITE_OK != ret) {
        if (NULL == sqlite3_db) {
          // sqlite3 was unable to even allocate memory
          ret_val = JS_ThrowInternalError(ctx, "unable to open database (OOM)");
        } else {
          // get error details from DB before we close it
          fprintf(stderr, "sqlite3_open failed: %s / %s\n",
                  sqlite3_errstr(ret),
                  sqlite3_errmsg(sqlite3_db));
          ret_val = throw_sqlite3_error(ctx, sqlite3_db);
          fprintf(stderr, "calling sqlite3 close on failed db\n");
          (void)sqlite3_close_v2(sqlite3_db);
        }
        goto done;
    }
    db_obj = JS_NewObjectClass(ctx, js_sqlite3_database_class_id);
    JS_SetOpaque(db_obj, sqlite3_db);
    ret_val = db_obj;
done:
    JS_FreeCString(ctx, filename);
    return ret_val;
}

// (handle: Sqlite3Database) -> undefined
static JSValue js_sqlite3_close(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
  JSValue db_handle = argv[0];
  sqlite3 *sqlite3_db;

  sqlite3_db = JS_GetOpaque(db_handle, js_sqlite3_database_class_id);

  if (!sqlite3_db) {
    return JS_ThrowTypeError(ctx, "invalid sqlite3 database handle");
  }

  (void) sqlite3_close_v2(sqlite3_db);
  JS_SetOpaque(db_handle, NULL);
  return JS_UNDEFINED;
}

// (handle: Sqlite3Database, stmt: string) => Sqlite3Statement
static JSValue js_sqlite3_prepare(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
  JSValue db_handle = argv[0];
  JSValue stmt_str = argv[1];
  JSValue ret_val = JS_UNDEFINED;
  JSValue stmt_obj = JS_UNDEFINED;
  int ret;
  sqlite3 *sqlite3_db;
  sqlite3_stmt *stmt;
  const char *stmt_cstr;
  const char *tail;

  sqlite3_db = JS_GetOpaque(db_handle, js_sqlite3_database_class_id);

  if (!sqlite3_db) {
    return JS_ThrowTypeError(ctx, "invalid sqlite3 database handle");
  }

  stmt_cstr = JS_ToCString(ctx, stmt_str);

  if (!stmt_cstr) {
    ret_val = JS_ThrowTypeError(ctx, "invalid prepared statement, string expected");
    goto done;
  }

  ret = sqlite3_prepare_v3(sqlite3_db, stmt_cstr, (int) strlen(stmt_cstr), 0, &stmt, &tail);
  if (SQLITE_OK != ret) {
    ret_val = JS_ThrowTypeError(ctx, "unable to prepare");
    goto done;
  }
 
  stmt_obj = JS_NewObjectClass(ctx, js_sqlite3_statement_class_id);
  JS_SetOpaque(stmt_obj, stmt);
  ret_val = stmt_obj;
done:
  JS_FreeCString(ctx, stmt_cstr);
  return ret_val;
}


static JSValue js_sqlite3_finalize(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
  sqlite3_stmt *stmt;

  stmt = JS_GetOpaque(argv[0], js_sqlite3_statement_class_id);
  if (!stmt) {
    return JS_ThrowTypeError(ctx, "unable to finalize (not a statement)");
  }
  // FIXME: Check error code and warn?
  sqlite3_finalize(stmt);
  JS_SetOpaque(argv[0], NULL);
  return JS_UNDEFINED;
}

static int sql_exec_cb(void *cls, int numcol, char **res, char **colnames) {
    printf("got row with %d columns\n", numcol);
    return 0;
}

static JSValue js_sqlite3_exec(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
    sqlite3 *sqlite3_db;
    JSValue db_handle = argv[0];
    JSValue stmt_str = argv[1];
    const char *stmt_cstr = NULL;
    int res;
    char *errmsg = NULL;
    JSValue ret_val = JS_UNDEFINED;

    sqlite3_db = JS_GetOpaque(db_handle, js_sqlite3_database_class_id);
    if (!sqlite3_db) {
        JS_ThrowTypeError(ctx, "invalid sqlite3 database handle");
        goto exception;
    }
    stmt_cstr = JS_ToCString(ctx, stmt_str);
    if (!stmt_cstr) {
        goto exception;
    }
    res = sqlite3_exec(sqlite3_db, stmt_cstr, &sql_exec_cb, NULL, &errmsg);
    if (SQLITE_OK != res) {
        throw_sqlite3_error(ctx, sqlite3_db);
        goto exception;
    }
done:
    sqlite3_free(errmsg);
    JS_FreeCString(ctx, stmt_cstr);
    return ret_val;
exception:
    ret_val = JS_EXCEPTION;
    goto done;
}

static int find_param_index(sqlite3_stmt *stmt, const char *name)
{
    int param_index;
    char *prefixed_name = malloc(strlen(name) + 2);
    memcpy(prefixed_name + 1, name, strlen(name) + 1);

    prefixed_name[0] = '$';
    param_index = sqlite3_bind_parameter_index(stmt, prefixed_name);
    if (param_index) {
        goto done;
    }
    prefixed_name[0] = ':';
    param_index = sqlite3_bind_parameter_index(stmt, prefixed_name);
    if (param_index) {
        goto done;
    }
    prefixed_name[0] = '@';
    param_index = sqlite3_bind_parameter_index(stmt, prefixed_name);
    if (param_index) {
        goto done;
    }
done:
    free(prefixed_name);
    return param_index;
}

static int bind_from_object(JSContext *ctx, sqlite3_stmt *stmt, JSValueConst obj)
{
    JSValue val;
    int i;
    uint32_t len = 0; /* len of property table */
    JSPropertyEnum *tab;
    const char *key = NULL;
    int retval = 0;

    if (JS_IsUndefined(obj)) {
        return 0;
    }

    if (JS_GetOwnPropertyNames(ctx, &tab, &len, obj,
                               JS_GPN_STRING_MASK | JS_GPN_ENUM_ONLY) < 0) {
        JS_ThrowTypeError(ctx, "can't get property names");
        return -1;
    }

    for (i = 0; i < len; i++) {
        int param_index;
        val = JS_GetProperty(ctx, obj, tab[i].atom);
        if (JS_IsException(val))
            goto fail;
        key = JS_AtomToCString(ctx, tab[i].atom);
        if (!key) {
            goto fail;
        }
        param_index = find_param_index(stmt, key);
        if (0 == param_index) {
            // JS_ThrowTypeError(ctx, "unable to bind, named param '%s' not found", key);
            //goto fail;
            // We ignore parameters that are bound but not used.
            goto next;
        }
        if (JS_IsNull(val)) {
            sqlite3_bind_null(stmt, param_index);
            goto next;
        }
        if (JS_IsString(val)) {
            const char *cstr;
            cstr = JS_ToCString(ctx, val);
            sqlite3_bind_text(stmt, param_index, cstr, (int) strlen(cstr), SQLITE_TRANSIENT);
            JS_FreeCString(ctx, cstr);
            goto next;
        }
        if (JS_IsNumber(val)) {
            int64_t n;
            JS_ToInt64(ctx, &n, val);
            sqlite3_bind_int64(stmt, param_index, n);
            goto next;
        }
        if (JS_IsArrayBuffer(val)) {
            uint8_t *data;
            size_t size;
            data = JS_GetArrayBuffer(ctx, &size, val);
            if (!data) {
                goto fail;
            }
            sqlite3_bind_blob(stmt, param_index, data, (int) size, SQLITE_TRANSIENT);
            goto next;
        }
        JS_ThrowTypeError(ctx, "unable to bind, unsupported type for arg %s", key);
        goto fail;
next:
        JS_FreeCString(ctx, key);
        JS_FreeValue(ctx, val);
    }
done:
    for (i = 0; i < len; i++) {
        JS_FreeAtom(ctx, tab[i].atom);
    }
    js_free(ctx, tab);
    return retval;
fail:
    retval = -1;
    goto done;
}


static JSValue js_sqlite3_stmt_run(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
    JSValue ret_val = JS_UNDEFINED;
    JSValue stmt_handle = argv[0];
    sqlite3_stmt *stmt;
    sqlite3 *db;
    int sqlret;

    stmt = JS_GetOpaque(stmt_handle, js_sqlite3_statement_class_id);
    if (!stmt) {
        ret_val = JS_ThrowTypeError(ctx, "invalid sqlite3 database handle");
        goto done;
    }
    db = sqlite3_db_handle(stmt);
    // Reset is not strictly necessary, we do it anyway just to be safe
    sqlret = sqlite3_reset(stmt);
    if (SQLITE_OK != sqlret) {
        fprintf(stderr, "sqlite3_reset failed (in stmt_run): %s\n", sqlite3_errmsg(db));
        ret_val = JS_ThrowTypeError(ctx, "failed to reset");
        goto done;
    }
    sqlret = sqlite3_clear_bindings(stmt);
    if (SQLITE_OK != sqlret) {
        ret_val = JS_ThrowTypeError(ctx, "failed to clear bindings");
        goto done;
    }
    if (argc > 1) {
        if (0 != bind_from_object(ctx, stmt, argv[1])) {
            ret_val = JS_EXCEPTION;
            goto done;
        }
    }
    while (1) {
        sqlret = sqlite3_step(stmt);
        switch (sqlret) {
        case SQLITE_ROW:
            break;
        case SQLITE_DONE: {
            JSValue rowid_val;
            ret_val = JS_NewObject(ctx);
            sqlite3_int64 rowid = sqlite3_last_insert_rowid(db);
            if (rowid >= MIN_SAFE_INTEGER && rowid <= MAX_SAFE_INTEGER) {
                rowid_val = JS_NewInt64(ctx, rowid);
            } else {
                rowid_val = JS_NewBigInt64(ctx, rowid);
            }
            JS_SetPropertyStr(ctx, ret_val, "lastInsertRowid", rowid_val);
            sqlret = sqlite3_reset(stmt);
            if (SQLITE_OK != sqlret) {
                fprintf(stderr, "sqlite3_reset failed (in stmt_run after SQLITE_DONE): %s\n", sqlite3_errmsg(db));
                JS_FreeValue(ctx, ret_val);
                ret_val = JS_ThrowTypeError(ctx, "failed to reset");
                goto done;
            }
            goto done;
        }
        default:
            ret_val = throw_sqlite3_error(ctx, db);
            sqlite3_reset(stmt);
            goto done;
        }
    }
done:
    return ret_val;
}


static int extract_result_row(JSContext *ctx, sqlite3_stmt *stmt, JSValueConst target)
{
    int colcount;
    int i;

    colcount = sqlite3_column_count(stmt);

    for (i = 0; i < colcount; i++) {
        const char *colname = sqlite3_column_name(stmt, i);
        int coltype = sqlite3_column_type(stmt, i);
        switch (coltype) {
        case SQLITE_INTEGER: {
            int64_t val = sqlite3_column_int64(stmt, i);
            if (val > MAX_SAFE_INTEGER || val < MIN_SAFE_INTEGER) {
                JS_SetPropertyStr(ctx, target, colname, JS_NewBigInt64(ctx, val));
            } else {
                JS_SetPropertyStr(ctx, target, colname, JS_NewInt64(ctx, val));
            }
            break;
        }
        case SQLITE_FLOAT: {
            double val = sqlite3_column_double(stmt, i);
            JS_SetPropertyStr(ctx, target, colname, JS_NewFloat64(ctx, val));
            break;
        }
        case SQLITE_BLOB: {
            JSValue abuf;
            const uint8_t *blobdata = sqlite3_column_blob(stmt, i);
            size_t bloblen = sqlite3_column_bytes(stmt, i);
            abuf = JS_NewArrayBufferCopy(ctx, blobdata, bloblen);
            JS_SetPropertyStr(ctx, target, colname, JS_NewTypedArray(ctx, abuf, 1));
            break;
        }
        case SQLITE_NULL:
            JS_SetPropertyStr(ctx, target, colname, JS_NULL);
            break;
        case SQLITE_TEXT: {
            const char *text = (const char *) sqlite3_column_text(stmt, i);
            JS_SetPropertyStr(ctx, target, colname, JS_NewString(ctx, text));
            break;
        }
        default:
            JS_ThrowInternalError(ctx, "unexpected type from DB");
            return -1;
        }
    }
    return 0;
}


static JSValue js_sqlite3_stmt_get_all(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
    JSValue ret_val = JS_UNDEFINED;
    JSValue stmt_handle = argv[0];
    sqlite3_stmt *stmt;
    sqlite3 *db;
    int sqlret;
    JSValue rows_array = JS_UNDEFINED;

    stmt = JS_GetOpaque(stmt_handle, js_sqlite3_statement_class_id);
    if (!stmt) {
        ret_val = JS_ThrowTypeError(ctx, "invalid sqlite3 database handle");
        goto done;
    }
    db = sqlite3_db_handle(stmt);
    sqlret = sqlite3_reset(stmt);
    if (SQLITE_OK != sqlret) {
        fprintf(stderr, "sqlite3_reset failed (in stmt_get_all): %s\n", sqlite3_errmsg(db));
        ret_val = JS_ThrowTypeError(ctx, "failed to reset");
        goto done;
    }
    sqlret = sqlite3_clear_bindings(stmt);
    if (SQLITE_OK != sqlret) {
        ret_val = JS_ThrowTypeError(ctx, "failed to clear bindings");
        goto done;
    }
    if (argc > 1) {
        if (0 != bind_from_object(ctx, stmt, argv[1])) {
            ret_val = JS_EXCEPTION;
            goto done;
        }
    }
    rows_array = JS_NewArray(ctx);
    while (1) {
        sqlret = sqlite3_step(stmt);
        switch (sqlret) {
        case SQLITE_ROW: {
            JSValue row_obj = JS_NewObject(ctx);
            if (0 != extract_result_row(ctx, stmt, row_obj)) {
                goto fail;
            }
            qjs_array_append_new(ctx, rows_array, row_obj);
            break;
        }
        case SQLITE_DONE: {
            sqlret = sqlite3_reset(stmt);
            if (SQLITE_OK != sqlret) {
                fprintf(stderr, "sqlite3_reset failed (in stmt_get_all after SQLITE_DONE): %s\n", sqlite3_errmsg(db));
                ret_val = JS_ThrowTypeError(ctx, "failed to reset");
            } else {
                ret_val = JS_DupValue(ctx, rows_array);
            }
            goto done;
        }
        default:
            ret_val = throw_sqlite3_error(ctx, db);
            sqlite3_reset(stmt);
            goto done;
        }
    }
done:
    JS_FreeValue(ctx, rows_array);
    return ret_val;
fail:
    ret_val = JS_EXCEPTION;
    goto done;
}

static JSValue js_sqlite3_stmt_get_first(JSContext *ctx, JSValue this_val,
                                              int argc, JSValueConst *argv)
{
  JSValue ret_val = JS_UNDEFINED;
    JSValue stmt_handle = argv[0];
    sqlite3_stmt *stmt;
    sqlite3 *db;
    int sqlret;

    stmt = JS_GetOpaque(stmt_handle, js_sqlite3_statement_class_id);
    if (!stmt) {
        ret_val = JS_ThrowTypeError(ctx, "invalid sqlite3 database handle");
        goto done;
    }
    db = sqlite3_db_handle(stmt);
    sqlret = sqlite3_reset(stmt);
    if (SQLITE_OK != sqlret) {
        fprintf(stderr, "sqlite3_reset failed (in stmt_get_first): %s\n", sqlite3_errmsg(db));
        ret_val = JS_ThrowTypeError(ctx, "failed to reset");
        goto done;
    }
    sqlret = sqlite3_clear_bindings(stmt);
    if (SQLITE_OK != sqlret) {
        ret_val = JS_ThrowTypeError(ctx, "failed to clear bindings");
        goto done;
    }
    if (argc > 1) {
        if (0 != bind_from_object(ctx, stmt, argv[1])) {
            ret_val = JS_EXCEPTION;
            goto done;
        }
    }
    while (1) {
        sqlret = sqlite3_step(stmt);
        switch (sqlret) {
        case SQLITE_ROW: {
            JSValue row_obj = JS_NewObject(ctx);
            if (0 != extract_result_row(ctx, stmt, row_obj)) {
                goto fail;
            }
            ret_val = row_obj;
            goto done;
        }
        case SQLITE_DONE: {
            ret_val = JS_UNDEFINED;
            goto done;
        }
        default:
            ret_val = throw_sqlite3_error(ctx, db);
            goto done;
        }
    }
reset:
    sqlret = sqlite3_reset(stmt);
    if (SQLITE_OK != sqlret) {
        fprintf(stderr, "sqlite3_reset failed (in stmt_get_first): %s\n", sqlite3_errmsg(db));
        JS_FreeValue(ctx, ret_val);
        ret_val = JS_ThrowTypeError(ctx, "failed to reset");
    }
done:
    return ret_val;
fail:
    ret_val = JS_EXCEPTION;
    goto done;
}


static const JSCFunctionListEntry tart_talercrypto_funcs[] = {
    JS_CFUNC_DEF("structuredClone", 1, js_structured_clone),
    JS_CFUNC_DEF("encodeUtf8", 1, js_encode_utf8),
    JS_CFUNC_DEF("decodeUtf8", 1, js_decode_utf8),
    JS_CFUNC_DEF("randomBytes", 1, js_random_bytes),
    JS_CFUNC_DEF("encodeCrock", 1, js_talercrypto_encode_crock),
    JS_CFUNC_DEF("decodeCrock", 1, js_talercrypto_decode_crock),
    JS_CFUNC_DEF("hash", 1, js_talercrypto_hash),
    JS_CFUNC_DEF("hashStateInit", 0, js_talercrypto_hash_state_init),
    JS_CFUNC_DEF("hashStateUpdate", 2, js_talercrypto_hash_state_update),
    JS_CFUNC_DEF("hashStateFinish", 1, js_talercrypto_hash_state_finish),
    JS_CFUNC_DEF("hashArgon2id", 5, js_talercrypto_hash_argon2id),
    JS_CFUNC_DEF("eddsaGetPublic", 1, js_talercrypto_eddsa_key_get_public),
    JS_CFUNC_DEF("ecdheGetPublic", 1, js_talercrypto_ecdhe_key_get_public),
    JS_CFUNC_DEF("eddsaSign", 2, js_talercrypto_eddsa_sign),
    JS_CFUNC_DEF("eddsaVerify", 3, js_talercrypto_eddsa_verify),
    JS_CFUNC_DEF("kdf", 3, js_talercrypto_kdf),
    JS_CFUNC_DEF("keyExchangeEcdhEddsa", 2, js_talercrypto_kx_ecdh_eddsa),
    JS_CFUNC_DEF("keyExchangeEddsaEcdh", 2, js_talercrypto_kx_eddsa_ecdh),
    JS_CFUNC_DEF("rsaBlind", 3, js_talercrypto_rsa_blind),
    JS_CFUNC_DEF("rsaUnblind", 3, js_talercrypto_rsa_unblind),
    JS_CFUNC_DEF("rsaVerify", 3, js_talercrypto_rsa_verify),
    JS_CFUNC_DEF("sqlite3Open", 1, js_sqlite3_open),
    JS_CFUNC_DEF("sqlite3Close", 1, js_sqlite3_close),
    JS_CFUNC_DEF("sqlite3Prepare", 2, js_sqlite3_prepare),
    JS_CFUNC_DEF("sqlite3Finalize", 1, js_sqlite3_finalize),
    JS_CFUNC_DEF("sqlite3Exec", 2, js_sqlite3_exec),
    JS_CFUNC_DEF("sqlite3StmtRun", 2, js_sqlite3_stmt_run),
    JS_CFUNC_DEF("sqlite3StmtGetFirst", 2, js_sqlite3_stmt_get_first),
    JS_CFUNC_DEF("sqlite3StmtGetAll", 2, js_sqlite3_stmt_get_all),
};

static int tart_talercrypto_init(JSContext *ctx, JSModuleDef *m)
{    
    /* create the HashState class */
    JS_NewClassID(&js_hash_state_class_id);
    JS_NewClass(JS_GetRuntime(ctx), js_hash_state_class_id, &js_hash_state_class);

    /* create the Sqlite3Database class*/
    JS_NewClassID(&js_sqlite3_database_class_id);
    JS_NewClass(JS_GetRuntime(ctx), js_sqlite3_database_class_id, &js_sqlite3_database_class);

    /* create the Sqlite3Statement class*/
    JS_NewClassID(&js_sqlite3_statement_class_id);
    JS_NewClass(JS_GetRuntime(ctx), js_sqlite3_statement_class_id, &js_sqlite3_statement_class);

    return JS_SetModuleExportList(ctx, m, tart_talercrypto_funcs,
                                  countof(tart_talercrypto_funcs));
}

JSModuleDef *tart_init_module_talercrypto(JSContext *ctx, const char *module_name)
{
    JSModuleDef *m;
    m = JS_NewCModule(ctx, module_name, tart_talercrypto_init);
    if (!m)
        return NULL;
    JS_AddModuleExportList(ctx, m, tart_talercrypto_funcs,
                           countof(tart_talercrypto_funcs));
    return m;
}
