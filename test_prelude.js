const enc = new TextEncoder();
const dec = new TextDecoder();

const buf = enc.encode("Hello, World");

console.log("byteOffset", buf.byteOffset);
console.log("byteLength", buf.byteLength);

const str = dec.decode(buf);

if (str != "Hello, World") {
  throw Error("test 1 failed");
}

// Put 4 bytes before and after!
const buf2 = new Uint8Array(buf.byteLength + 8);

buf2.set(buf, 4);

buf2[0] = 42;

console.log(JSON.stringify(dec.decode(buf2.slice(3, buf2.byteLength - 4))))

const str2 = dec.decode(buf2.slice(4, buf2.byteLength - 4))
if (str2 != "Hello, World") {
  throw Error("test 2 failed");
}
