#!/usr/bin/env bash
set -e
set -x

# Install dependencies
apt-get install -y --allow-downgrades \
        clang \
        curl \
        default-jdk-headless \
        git \
        gnupg \
        jq \
        make \
        meson \
        npm \
        unzip \
        wget \
        zip

# Needed to build wallet-core
npm install -g pnpm@8.6.3

# Install Android SDK Manager
wget --no-verbose -O cmdline-tools.zip \
     https://dl.google.com/android/repository/commandlinetools-linux-11479570_latest.zip
unzip cmdline-tools.zip
rm cmdline-tools.zip
mkdir -p ${ANDROID_HOME}/cmdline-tools
mv cmdline-tools ${ANDROID_HOME}/cmdline-tools/latest

# Accept all those nasty EULAs
mkdir -p ${ANDROID_HOME}/licenses/
printf "\n24333f8a63b6825ea9c5514f83c2829b004d1fee" > ${ANDROID_HOME}/licenses/android-sdk-license

# Install platform-tools and ndk-bundle (for stripping libraries)
mkdir /root/.android
touch /root/.android/repositories.cfg
echo y | $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager "platform-tools" "ndk-bundle"

# Add $ANDROID_HOME and $ANDROID_SDK_ROOT to PATH
# printf 'export PATH=$PATH:%s' $ANDROID_HOME/cmdline-tools/latest/bin > ~/.bashrc
printf 'export ANDROID_SDK_ROOT=%s' $ANDROID_HOME >> /etc/environment
