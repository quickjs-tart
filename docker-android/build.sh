#!/usr/bin/env bash
set -e
set -x

# Copy all the required files from volume
QJS_PATH=/opt/quickjs-tart
cp $QJS_PATH/docker-android/build.sh ./
cp -r $QJS_PATH/quickjs ./
cp -r $QJS_PATH/sqlite3 ./
cp -r $QJS_PATH/subprojects ./
cp -r $QJS_PATH/QuickJS-android ./
cp $QJS_PATH/meson.build ./
mkdir ./cross && cp $QJS_PATH/cross/build-android.sh ./cross/
cp $QJS_PATH/*.c ./
cp $QJS_PATH/*.h ./
cp $QJS_PATH/*.js ./

# Get wallet-core version from gradle.properties
GRADLE_PROPERTIES_FILE=./QuickJS-android/gradle.properties
WALLET_CORE_VERSION=$(grep -hoP '^WALLET_CORE_VERSION=\K.*' $GRADLE_PROPERTIES_FILE)

# Download and build wallet-core (as non-root)
git clone https://git.gnunet.org/taler-typescript-core.git
cd taler-typescript-core/
git checkout "tags/${WALLET_CORE_VERSION}"

npm i -g pnpm

useradd walletcore
chown -R walletcore:walletcore .
su walletcore <<EOF
./bootstrap
./configure
make embedded
EOF

cd ../
cp taler-typescript-core/packages/taler-wallet-embedded/dist/taler-wallet-core-qjs.mjs .
rm -rf taler-typescript-core/

# Build .so library
. /etc/environment
./cross/build-android.sh x86
./cross/build-android.sh x86_64
./cross/build-android.sh arm64-v8a
./cross/build-android.sh armeabi-v7a

ANDROID_DIR=./QuickJS-android
JNILIBS_DIR=$ANDROID_DIR/qtart/src/main/jniLibs
cp ./build-android-x86/libtalerwalletcore.so $JNILIBS_DIR/x86
cp ./build-android-x86_64/libtalerwalletcore.so $JNILIBS_DIR/x86_64
cp ./build-android-arm64-v8a/libtalerwalletcore.so $JNILIBS_DIR/arm64-v8a
cp ./build-android-armeabi-v7a/libtalerwalletcore.so $JNILIBS_DIR/armeabi-v7a
cd $ANDROID_DIR

# Build or publish .aar library
if [ -z "${MAVEN_PUBLISH+x}" ]; then
    echo "Publishing to local maven"
    ./gradlew --build-cache --gradle-user-home /root/gradle-cache --console=plain publishToMavenLocal
else
    ./gradlew publishAllPublicationsToMavenCentralRepository
    ./gradlew closeAndReleaseRepository
fi
