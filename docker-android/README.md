# Docker build instructions

At Taler, we're deeply concerned about the reproducibility of our
builds, and that's why we make use of Docker in order to ensure that
our builds are consistent and replicable.

## 1. Environment variables

So as not to store sensitive data such as private keys and login
credentials (required for publishing to Maven Central) in our build
scripts, we make use of an environment file not checked into the
repository. In order to provide the necessary parameters to the Docker
container, do the following:

1. Rename the `env.example` file to `.env`.
2. Fill-in the variables as instructed in the comments.

## 2. Build image and run build

In order to automate even more the automation that the Docker image
itself already provides, we make use of Docker Compose. The provided
`docker-compose.yml` file will build the image, pick up the
environment file and mount a volume that exposes the container's local
Maven repository, so that you can test the artifacts before publishing
them. All you have to do, is the following:

```bash
cd docker-android/
docker-compose up --build
```

## 3. Test Maven artifact locally

Testing in production is not really a great idea. It is recommended
that you first test your artifacts locally, before publishing them to
Maven Central. Doing this will require modifying the top-level
`build.gradle` of the Android app that uses this artifact:

```groovy
allprojects {
    repositories {
        maven {
            url '<absolute-path-to-quickjs-tart>/.m2/repository/'
        }
        // ...
    }
}
```

Now, add the dependency to the module-level `build.gradle`:

```groovy
dependencies {
    // ...
    implementation "net.taler.qtart:<version>@aar"
    // ...
}
```
