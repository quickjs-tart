#!/usr/bin/env bash
set -e
set -x

# # use snapshot repos for deterministic package versions
# DATE="20230220T144122Z"
# cat << EOF > /etc/apt/sources.list
# deb http://snapshot.debian.org/archive/debian/${DATE}/ bookworm main
# deb http://snapshot.debian.org/archive/debian-security/${DATE}/ bookworm-security main
# EOF

# # ignore expired package releases (they expire too fast)
echo 'Acquire::Check-Valid-Until "0";' >> /etc/apt/apt.conf.d/10-ignore-expiry

# # do not install documentation to keep image small
echo "path-exclude=/usr/share/locale/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc
echo "path-exclude=/usr/share/man/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc
echo "path-exclude=/usr/share/doc/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc

# # update package sources
apt-get update
apt-get -y upgrade

# install of default-jdk-headless fails otherwise on *-slim image
mkdir -p /usr/share/man/man1
