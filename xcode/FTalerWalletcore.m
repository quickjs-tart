/*
 * This file is part of GNU Taler
 * (C) 2021 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
#import "FTalerWalletcore.h"

static void TALER_WALLET_test_handler(void *cls, const char *message)
{
    printf("%s\n", message);
}

struct TALER_WALLET_Instance *
TALER_WALLET_test(int wait)
{
    struct TALER_WALLET_Instance *twi = TALER_WALLET_create();

    TALER_WALLET_set_message_handler(twi, &TALER_WALLET_test_handler, NULL);

    TALER_WALLET_run(twi);

    TALER_WALLET_send_request(twi, "{\"operation\": \"init\", \"args\": {\"skipDefaults\": true}}");

    TALER_WALLET_send_request(twi, "{\"operation\": \"getVersion\"}");

    TALER_WALLET_send_request(twi, "{\"operation\": \"runIntegrationTest\", \"args\": {"
                              "\"amountToWithdraw\": \"KUDOS:3\", \"amountToSpend\": \"KUDOS:1\", "
                              "\"bankBaseUrl\": \"https://bank.demo.taler.net/demobanks/default/access-api/\", "
                              "\"exchangeBaseUrl\": \"https://exchange.demo.taler.net/\", "
                              "\"merchantBaseUrl\": \"https://backend.demo.taler.net/\", "
                              "\"merchantAuthToken\": \"secret-token:sandbox\"}}");
    if (wait) {
        // Wait for wallet thread to finish.
        // If we don't call this, main() exits
        // and the wallet thread is killed.
        TALER_WALLET_join(twi);
    }
    return twi;
}
