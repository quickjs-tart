/*
 * This file is part of GNU Taler
 * (C) 2021 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
#import <Foundation/Foundation.h>

//! Project version number for TalerWalletcore.
FOUNDATION_EXPORT double TalerWalletcoreVersionNumber;

//! Project version string for TalerWalletcore.
FOUNDATION_EXPORT const unsigned char TalerWalletcoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TalerWalletcore/PublicHeader.h>

#import "FTalerWalletcore-Bridging-Header.h"

struct TALER_WALLET_Instance *
TALER_WALLET_test(int wait);
