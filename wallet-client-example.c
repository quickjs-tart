/*
   This file is part of GNU Taler
   Copyright (C) 2014-2022 Taler Systems SA

   GNU Taler is free software; you can redistribute it and/or modify it under the
   terms of the GNU Affero General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License along with
   GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */


/**
 * Sample client for a TALER_WALLET_Instance.
 */
#include <stdlib.h>
#include <stdio.h>
#include "taler_wallet_core_lib.h"


void
my_handler(void *cls, const char *message)
{
    printf("got message: %s\n", message);
}

void
log_handler(void *cls, enum TALER_WALLET_LogLevel level, const char *tag, const char *msg)
{
  printf("got log message, level %d, tag %s: %s\n", (int) level, tag, msg);
}


int main(int argc, char **argv)
{
    struct TALER_WALLET_Instance *wh = TALER_WALLET_create();

    TALER_WALLET_set_message_handler(wh, &my_handler, NULL);
    TALER_WALLET_set_log_handler(wh, &log_handler, NULL);

    TALER_WALLET_run(wh);

    TALER_WALLET_send_request(wh, "{\"operation\": \"init\", \"args\": {\"skipDefaults\": true}}");

    TALER_WALLET_send_request(wh, "{\"operation\": \"getVersion\"}");

    //TALER_WALLET_send_request(wh, "{}");

    // Wait for wallet thread to finish.
    // If we don't call this, main() exits
    // and the wallet thread is killed.
    TALER_WALLET_join(wh);
}
