-keep class net.taler.qtart.** {*;}
-keep interface net.taler.qtart.** {*;}

-dontwarn java.awt.*
-keep class com.sun.jna.* {*;}
-keepclassmembers class * extends com.sun.jna.* {public*;}
-keep,includedescriptorclasses class * {
    native <methods>;
}