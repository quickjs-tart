import com.vanniktech.maven.publish.MavenPublishPluginExtension
import com.vanniktech.maven.publish.SonatypeHost

plugins {
    id("com.android.library")
    id("com.vanniktech.maven.publish")
    kotlin("android")
}

val walletCoreVersion = System.getenv()["WALLET_CORE_VERSION"]

android {
    compileSdk = 33
    defaultConfig {
        minSdk = 24
        targetSdk = 33

        ndk {
            abiFilters.add("armeabi-v7a")
            abiFilters.add("arm64-v8a")
            abiFilters.add("x86")
            abiFilters.add("x86_64")
        }

        consumerProguardFiles("proguard-rules.pro")
        buildConfigField("String", "WALLET_CORE_VERSION", "\"$walletCoreVersion\"")
    }
}

dependencies {
    implementation("net.java.dev.jna:jna:5.13.0@aar")
    androidTestImplementation("androidx.test:runner:1.5.2")
    androidTestImplementation("androidx.test:rules:1.5.0")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
}

signing {
    useGpgCmd()
}

// Only sign when MAVEN_PUBLISH is set in the environment
tasks.withType<Sign>().configureEach {
    onlyIf { System.getenv().containsKey("MAVEN_PUBLISH") }
}

extensions.getByType<MavenPublishPluginExtension>().apply {
    sonatypeHost = SonatypeHost.S01
}
