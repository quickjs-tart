package net.taler.qtart

import android.util.Log
import com.sun.jna.Memory
import com.sun.jna.Pointer
import com.sun.jna.StringArray
import java.util.concurrent.atomic.AtomicInteger
import net.taler.qtart.TalerWalletCore.TalerNative.*

object Networking {
    private val lastRequestId = AtomicInteger(0)

    // Add your pointer here to protect it from the GC!
    private val pointerReferences = mutableListOf<Pointer>()

    interface RequestHandler {
        fun handleRequest(req: RequestInfo, id: Int, sendResponse: (resp: ResponseInfo) -> Unit)
        fun cancelRequest(id: Int): Boolean
    }

    enum class RedirectMode(val value: Int) {
        Unknown(-1),

        /**
         * Handle redirects transparently.
         */
        Transparent(0),

        /**
         * Redirect status codes are returned to the client.
         * The client can choose to follow them manually (or not).
         */
        Manual(1),

        /**
         * All redirect status codes result in an error.
         */
        Error(2);

        companion object {
            fun fromValue(value: Int): RedirectMode =
                RedirectMode.values().find { it.value == value } ?: Unknown
        }
    }

    class RequestInfo(
        val url: String,
        val method: String,
        val headers: Array<String>,
        val redirectMode: RedirectMode,
        val timeoutMs: Long,
        val debug: Boolean,
        val body: ByteArray?,
    )

    class ResponseInfo(
        val requestId: Int,
        val status: Int,
        val errorMsg: String?,
        val headers: Array<String>,
        val body: ByteArray?,
    )

    /**
     * Function to create a new HTTP fetch request.
     * The request can still be configured until it is started.
     * An identifier for the request will be written to @a handle.
     *
     * @return negative number on error, positive request_id on success
     */
    internal fun httpCreateRequest(
        reqInfo: JSHttpRequestInfo,
        reqHandler: RequestHandler,
    ): Int {
        val requestId = lastRequestId.addAndGet(1)

        reqHandler.handleRequest(
            req = RequestInfo(
                url = reqInfo.url!!,
                method = reqInfo.method!!,
                headers = reqInfo.request_headers!!.getStringArray(0),
                redirectMode = RedirectMode.fromValue(reqInfo.redirect!!),
                timeoutMs = reqInfo.timeout_ms!!.toLong(),
                debug = reqInfo.debug != 0,
                body = if (reqInfo.req_body_len!! > 0) {
                    reqInfo.req_body!!.getByteArray(0, reqInfo.req_body_len!!)
                } else null,
            ),
            id = requestId,
        ) { resp ->
            val rawResp = JSHttpResponseInfo()
            rawResp.request_id = resp.requestId
            rawResp.status = resp.status
            rawResp.errmsg = resp.errorMsg ?: ""
            rawResp.response_headers = StringArray(resp.headers, false)
            rawResp.num_response_headers = resp.headers.size
            if (resp.body != null && resp.body.isNotEmpty()) {
                // Manually allocate and write memory
                val pointer = Memory(resp.body.size.toLong())
                pointer.write(0, resp.body, 0, resp.body.size)
                pointerReferences.add(pointer)
                rawResp.body = pointer
                rawResp.body_len = resp.body.size
            } else {
                rawResp.body_len = 0
            }

            Log.d("Networking", "Response to request $requestId ready to send to qtart")
            reqInfo.response_cb?.invoke(reqInfo.response_cb_cls!!, rawResp)
        }

        return requestId
    }

    /**
     * Cancel a request. The request_id will become invalid
     * and the callback won't be called with request_id.
     */
    internal fun httpCancelRequest(
        requestId: Int,
        handler: RequestHandler,
    ): Int {
        val success = handler.cancelRequest(requestId)
        return if (success) requestId else -requestId
    }
}