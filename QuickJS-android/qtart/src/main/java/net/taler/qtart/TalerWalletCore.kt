/*
 * This file is part of GNU Taler
 * (C) 2023 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

/*
 * This file is part of GNU Taler
 * (C) 2023 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.qtart

import com.sun.jna.Callback
import com.sun.jna.Library
import com.sun.jna.Native
import com.sun.jna.Pointer
import com.sun.jna.Structure

class TalerWalletCore {
    internal interface TalerNative : Library {
        companion object {
            val INSTANCE: TalerNative by lazy {
                Native.setProtected(true)
                Native.load("talerwalletcore", TalerNative::class.java)
            }
        }

        interface TALER_WALLET_MessageHandlerFn : Callback {
            fun invoke(handler_p: Pointer, message: String)
        }

        interface TALER_LogFn : Callback {
            fun invoke(cls: Pointer, stream: Int, msg: String)
        }

        fun TALER_WALLET_create(): Pointer
        fun TALER_WALLET_set_message_handler(
            twi: Pointer,
            handler_f: TALER_WALLET_MessageHandlerFn,
            handler_p: Pointer
        )

        fun TALER_WALLET_send_request(twi: Pointer, request: String): Int
        fun TALER_WALLET_run(twi: Pointer): Int
        fun TALER_WALLET_join(twi: Pointer)
        fun TALER_WALLET_destroy(twi: Pointer)
        fun TALER_start_redirect_std(logfn: TALER_LogFn, cls: Pointer)
        fun TALER_set_curl_http_client(twi: Pointer)

        /**
         * Native networking
         */

        class JSHttpRequestInfo: Structure() {
            @JvmField var response_cb: JSHttpResponseCb? = null
            @JvmField var response_cb_cls: Pointer? = null
            @JvmField var url: String? = null
            @JvmField var method: String? = null
            @JvmField var request_headers: Pointer? = null // Array<String>
            @JvmField var redirect: Int? = null
            @JvmField var timeout_ms: Int? = null
            @JvmField var debug: Int? = null
            @JvmField var req_body: Pointer? = null // ByteArray
            @JvmField var req_body_len: Int? = null

            override fun getFieldOrder() = mutableListOf(
                "response_cb",
                "response_cb_cls",
                "url",
                "method",
                "request_headers",
                "redirect",
                "timeout_ms",
                "debug",
                "req_body",
                "req_body_len",
            )
        }

        class JSHttpResponseInfo: Structure() {
            @JvmField var request_id: Int? = null
            @JvmField var status: Int? = null
            @JvmField var errmsg: String? = null
            @JvmField var response_headers: Pointer? = null
            @JvmField var num_response_headers: Int? = null
            @JvmField var body: Pointer? = null
            @JvmField var body_len: Int? = null

            override fun getFieldOrder() = mutableListOf(
                "request_id",
                "status",
                "errmsg",
                "response_headers",
                "num_response_headers",
                "body",
                "body_len",
            )
        }

        interface JSHttpResponseCb: Callback {
            fun invoke(cls: Pointer, resp: JSHttpResponseInfo)
        }

        interface JSHttpRequestCb: Callback {
            fun invoke(cls: Pointer, req_info: JSHttpRequestInfo)
        }

        interface JSHttpReqCreateFn: Callback {
            fun invoke(cls: Pointer, req_info: JSHttpRequestInfo): Int
        }

        interface JSHttpReqCancelFn: Callback {
            fun invoke(cls: Pointer, request_id: Int): Int
        }

        fun TALER_set_http_client_implementation(twi: Pointer, impl: Pointer)

        fun TALER_pack_http_client_implementation(
            req_create: JSHttpReqCreateFn,
            req_cancel: JSHttpReqCancelFn,
            handler_p: Pointer,
        ): Pointer
    }

    private val walletInstance: Pointer = TalerNative.INSTANCE.TALER_WALLET_create()

    // Handlers must be here, so that GC doesn't wipe them!
    // Please don't refactor them, or else you'll suffer!
    private var currentMessageHandler: TalerNative.TALER_WALLET_MessageHandlerFn? = null
    private var currentLogHandler: TalerNative.TALER_LogFn? = null
    private var currentReqCreateHandler: TalerNative.JSHttpReqCreateFn? = null
    private var currentReqCancelHandler: TalerNative.JSHttpReqCancelFn? = null
    private var currentResHandler: TalerNative.JSHttpResponseCb? = null

    fun setMessageHandler(handler: (msg: String) -> Unit) {
        this.currentMessageHandler = object : TalerNative.TALER_WALLET_MessageHandlerFn {
            override fun invoke(handler_p: Pointer, message: String) {
                handler(message)
            }
        }
        TalerNative.INSTANCE.TALER_WALLET_set_message_handler(
            walletInstance,
            this.currentMessageHandler!!,
            walletInstance
        )
    }

    fun setStdoutHandler(handler: (msg: String) -> Unit) {
        this.currentLogHandler = object : TalerNative.TALER_LogFn {
            override fun invoke(cls: Pointer, stream: Int, msg: String) {
                handler(msg)
            }
        }
        TalerNative.INSTANCE.TALER_start_redirect_std(this.currentLogHandler!!, walletInstance)
    }

    fun sendRequest(request: String) {
        TalerNative.INSTANCE.TALER_WALLET_send_request(walletInstance, request)
    }

    fun setCurlHttpClient() {
        TalerNative.INSTANCE.TALER_set_curl_http_client(walletInstance)
    }

    fun run() {
        TalerNative.INSTANCE.TALER_WALLET_run(walletInstance)
    }

    fun destroy() {
        TalerNative.INSTANCE.TALER_WALLET_destroy(walletInstance)
    }

    fun join() {
        TalerNative.INSTANCE.TALER_WALLET_join(walletInstance)
    }

    /**
     * Native networking
     */

    fun setHttpClient(handler: Networking.RequestHandler) {
        this.currentReqCreateHandler = object: TalerNative.JSHttpReqCreateFn {
            override fun invoke(cls: Pointer, req_info: TalerNative.JSHttpRequestInfo): Int {
                // TODO: return positive request_id on success, negative on failure
                //  right now, this can't be done synchronously; doing it async
                //  might require a modification in the C API.
                return Networking.httpCreateRequest(req_info, handler)
            }
        }
        this.currentReqCancelHandler = object: TalerNative.JSHttpReqCancelFn {
            override fun invoke(cls: Pointer, request_id: Int): Int {
                return Networking.httpCancelRequest(request_id, handler)
            }
        }

        val client = TalerNative.INSTANCE.TALER_pack_http_client_implementation(
            this.currentReqCreateHandler!!,
            this.currentReqCancelHandler!!,
            this.walletInstance,
        )

        TalerNative.INSTANCE.TALER_set_http_client_implementation(walletInstance, client)
    }
}
