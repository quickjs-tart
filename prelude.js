// The prelude defines basic functionality
// that is expected by the Taler wallet core JavaScript,
// but not provided by quickjs or the "tart" module directly.

import * as os from "os";
import * as tart from "tart";

class TextEncoder {
  encode(str) {
    return new Uint8Array(tart.encodeUtf8(str));
  }
}

class TextDecoder {
  decode(bytes) {
    return tart.decodeUtf8(bytes);
  }
}

globalThis.TextEncoder = TextEncoder;
globalThis.TextDecoder = TextDecoder;
globalThis.setTimeout = (f, t) => os.setTimeout(f, t);
globalThis.clearTimeout = (h) => os.clearTimeout(h);
globalThis.setImmediate = (f) => os.setTimeout(f, 0);

// FIXME: log to the right streams!
console.info = (...args) => { console.log(...args); };
console.warn = (...args) => { console.log(...args); };
console.error = (...args) => { console.log(...args); };
console.assert = (b) => { if (!b) throw Error("assertion failed") };

globalThis._tart = tart;
