/*
 This file is part of GNU Taler
 Copyright (C) 2014-2022 Taler Systems SA

 GNU Taler is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free Software
 Foundation; either version 3, or (at your option) any later version.

 GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along with
 GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

/**
 * C interface to the functionality of wallet-core.
 *
 * Currently, the underlying implementation uses the JS implementation of
 * wallet-core, but this may (or may not) change in the future.
 *
 * @author Florian Dold
 */
#ifndef _TALER_WALLET_LIB_H
#define _TALER_WALLET_LIB_H

/*
 This file is part of GNU Taler
 Copyright (C) 2024 Taler Systems SA

 GNU Taler is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free Software
 Foundation; either version 3, or (at your option) any later version.

 GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along with
 GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */


// ## Native HTTP client library support.

// Considerations:
// - the API is designed for the HTTP client implementation
//   to run in its own thread and *not* be integrated with the
//   application's main event loop.
// - focus on small API
// - not a generic HTTP client, only supposed to serve the needs
//   of a JS runtime
// - only very tiny subset of HTTP supported
// - no request/response streaming
// - should be appropriate to implement a JS HTTP fetch function
//   in the style of WHATWG fetch
// - no focus on ABI compatibility whatsoever


#ifndef _QUICKJS_HTTP_H
#define _QUICKJS_HTTP_H

#include <stdint.h>
#include <limits.h>
#include <stddef.h>

// Forward declaration;
struct JSHttpResponseInfo;

/**
 * Callback called when an HTTP response has arrived.
 *
 * IMPORTANT: May be called from an arbitrary thread.
 */
typedef void (*JSHttpResponseCb)(void *cls, struct JSHttpResponseInfo *resp);

enum JSHttpRedirectFlag {
  /**
   * Handle redirects transparently.
   */
  JS_HTTP_REDIRECT_TRANSPARENT = 0,
  /**
   * Redirect status codes are returned to the client.
   * The client can choose to follow them manually (or not).
   */
  JS_HTTP_REDIRECT_MANUAL = 1,
  /**
   * All redirect status codes result in an error.
   */
  JS_HTTP_REDIRECT_ERROR = 2,
};

/**
 * Info needed to start a new HTTP request.
 */
struct JSHttpRequestInfo {
  /**
   * Callback called with the response for the request.
   */
  JSHttpResponseCb response_cb;

  /**
   * Closure for response_cb.
   */
  void *response_cb_cls;

  /**
   * Request URL.
   */
  const char *url;

  /**
   * Request method.
   */
  const char *method;

  /**
   * Null-terminated array of request headers.
   */
  char **request_headers;

  /**
   * 0: Handle redirects transparently.
   * 1: Handle redirects manually.
   * 2: Redirects result in an error.
   */
  enum JSHttpRedirectFlag redirect;

  /**
   * Request timeout in milliseconds.
   *
   * When 0 is specified, the timeout is the default request
   * timeout for the platform.
   *
   * When -1 is specified, there is no timeout.  This might not be
   * supported on all platforms.
   */
  int timeout_ms;

  /**
   * Enable debug output for this request.
   */
  int debug;

  /**
   * Request body or NULL.
   */
  void *req_body;

  /**
   * Length or request body or 0.
   */
  size_t req_body_len;
};

/**
 * Contents of an HTTP response.
 */
struct JSHttpResponseInfo {

  /**
   * Request that this is a response to.
   *
   * (Think of the request ID like a file descriptor number).
   */
  int request_id;

  /**
   * HTTP response status code or 0 on error.
   */
  int status;

  /**
   * When status is 0, error message.
   */
  char *errmsg;

  /**
   * Array of `num_response_headers` response headers.
   */
  char **response_headers;

  /**
   * Number of response headers.
   */
  int num_response_headers;

  /**
   * Response body or NULL.
   */
  void *body;

  /**
   * Length of the response body or 0.
   */
  size_t body_len;
};

/**
 * Callback called when an HTTP response has arrived.
 *
 * IMPORTANT: May be called from an arbitrary thread.
 */
typedef void (*JSHttpResponseCb)(void *cls, struct JSHttpResponseInfo *resp);

/**
 * Function to create a new HTTP fetch request.
 * The request can still be configured until it is started.
 * An identifier for the request will be written to @a handle.
 *
 * @return negative number on error, positive request_id on success
 */
typedef int (*JSHttpReqCreateFn)(void *cls, struct JSHttpRequestInfo *req_info);

/**
 * Cancel a request. The request_id will become invalid
 * and the callback won't be called with request_id.
 */
typedef int (*JSHttpReqCancelFn)(void *cls, int request_id);

struct JSHttpClientImplementation {
  /**
   * Opaque closure passed to client functions.
   */
  void *cls;
  JSHttpReqCreateFn req_create;
  JSHttpReqCancelFn req_cancel;
};

struct JSHttpClientImplementation *
js_curl_http_client_create(void);

void
js_curl_http_client_destroy(struct JSHttpClientImplementation *impl);

#endif /* _QUICKJS_HTTP_H */

/**
 * Opaque handle to a Taler wallet-core instance.
 */
struct TALER_WALLET_Instance;

/**
 * Handler for messages from the wallet.
 *
 * @param handler_p opaque closure for the message handler
 * @param message message from wallet-core as a JSON string
 */
typedef void (*TALER_WALLET_MessageHandlerFn)(void *handler_p, const char *message);

enum TALER_WALLET_LogLevel {
    TALER_WALLET_LOG_TRACE = 1,
    TALER_WALLET_LOG_INFO = 2,
    TALER_WALLET_LOG_MESSAGE = 3,
    TALER_WALLET_LOG_WARN = 4,
    TALER_WALLET_LOG_ERROR = 5
};

/**
 * Handler for log message from wallet-core.
 *
 * @param log_p opaque closure for the log handler
 * @param level log level of the log message
 * @param tag log tag (usually the file from which the message gets logged)
 * @param msg the log message
 */
typedef void (*TALER_WALLET_LogHandlerFn)(void *log_p,
                                          enum TALER_WALLET_LogLevel level,
                                          const char *tag,
                                          const char *msg);

/**
 * Create a new wallet-core instance..
 */
struct TALER_WALLET_Instance *
TALER_WALLET_create(void);

/**
 * Set a handler for notification and response messages.
 * Must be called before the wallet runs.
 *
 * Caution: The handler will be called from a different thread.
 */
void
TALER_WALLET_set_message_handler(struct TALER_WALLET_Instance *twi,
                                 TALER_WALLET_MessageHandlerFn handler_f,
                                 void *handler_p);

/**
 * Set a handler for log messages from wallet-core.
 * Must be called before the wallet runs.
 *
 * Caution: The log message handler will be called from a different thread.
 */
void
TALER_WALLET_set_log_handler(struct TALER_WALLET_Instance *twi,
                             TALER_WALLET_LogHandlerFn handler_f,
                             void *handler_p);

/**
 * Set/override the JS file with the wallet-core implementation.
 * Must be called before the wallet runs.
 */
// FIXME: Not implemented!
//void
//TALER_WALLET_set_jsfile(struct TALER_WALLET_Instance *twi,
//                        const char *filename);

/**
 * Send a message to wallet-core.
 *
 * Responses will be sent asynchronously to the message handler
 * set with #TALER_WALLET_set_message_handler.
 */
int
TALER_WALLET_send_request(struct TALER_WALLET_Instance *twi,
                          const char *request);

/**
 * Run wallet-core in a thread.
 *
 * This function creates a new thread and returns immediately.
 *
 * Returns 0 on success or a non-zero error code otherwise.
 */
int
TALER_WALLET_run(struct TALER_WALLET_Instance *twi);

/**
 * Block until the wallet returns.
 */
void
TALER_WALLET_join(struct TALER_WALLET_Instance *twi);

/**
 * Destroy the wallet handle and free resources associated with it.
 *
 * Note that for a graceful shutdown of the wallet,
 * an appropriate shutdown message should be sent first,
 * and destroy() should only be called after the wallet has
 * sent a response to the shutdown message.
 */
//void
//TALER_WALLET_destroy(struct TALER_WALLET_Instance *twi);

/**
 * Handler for messages that should be logged.
 *
 * @param stream NOT YET IMPLEMENTED: indicator for the stream that
 *               the message is coming from,
 */
typedef void (*TALER_LogFn)(void *handler_p, int stream, const char *msg);

/**
 * Redirect stderr and stdout to a function.
 *
 * Workaround for platforms where stderr is not visible in logs.
 *
 * @return 0 on success, error code otherwise
 */
int
TALER_start_redirect_std(TALER_LogFn logfn, void *handler_p);

/**
 * Set the HTTP client implementation to be used by the wallet.
 *
 * @param twi wallet-core instance
 * @param impl HTTP client implementation
 */
void
TALER_set_http_client_implementation(struct TALER_WALLET_Instance *twi,
                                     struct JSHttpClientImplementation *impl);

#pragma mark -
/**
 * Build JSHttpClientImplementation struct for native HTTP client implementation to be used by the wallet.
 *
 * @param req_create HTTP client implementation
 * @param req_cancel HTTP client implementation
 * @param handler_p Pointer to Handler's "this"
 */
struct JSHttpClientImplementation *
TALER_pack_http_client_implementation(JSHttpReqCreateFn req_create,
                                      JSHttpReqCancelFn req_cancel,
                                      void *handler_p);

#endif /*_TALER_WALLET_LIB_H */
