/*
 This file is part of GNU Taler
 Copyright (C) 2014-2022 Taler Systems SA

 GNU Taler is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free Software
 Foundation; either version 3, or (at your option) any later version.

 GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along with
 GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

/**
 * C interface to the functionality of wallet-core.
 *
 * Currently, the underlying implementation uses the JS implementation of
 * wallet-core, but this may (or may not) change in the future.
 *
 * @author Florian Dold
 */
#ifndef _TALER_WALLET_LIB_H
#define _TALER_WALLET_LIB_H

#include "quickjs/quickjs-http.h"

/**
 * Opaque handle to a Taler wallet-core instance.
 */
struct TALER_WALLET_Instance;

/**
 * Handler for messages from the wallet.
 *
 * @param handler_p opaque closure for the message handler
 * @param message message from wallet-core as a JSON string
 */
typedef void (*TALER_WALLET_MessageHandlerFn)(void *handler_p, const char *message);

enum TALER_WALLET_LogLevel {
    TALER_WALLET_LOG_TRACE = 1,
    TALER_WALLET_LOG_INFO = 2,
    TALER_WALLET_LOG_MESSAGE = 3,
    TALER_WALLET_LOG_WARN = 4,
    TALER_WALLET_LOG_ERROR = 5
};

/**
 * Handler for log message from wallet-core.
 *
 * @param log_p opaque closure for the log handler
 * @param level log level of the log message
 * @param tag log tag (usually the file from which the message gets logged)
 * @param msg the log message
 */
typedef void (*TALER_WALLET_LogHandlerFn)(void *log_p,
                                          enum TALER_WALLET_LogLevel level,
                                          const char *tag,
                                          const char *msg);

/**
 * Create a new wallet-core instance..
 */
struct TALER_WALLET_Instance *
TALER_WALLET_create(void);

/**
 * Set a handler for notification and response messages.
 * Must be called before the wallet runs.
 *
 * Caution: The handler will be called from a different thread.
 */
void
TALER_WALLET_set_message_handler(struct TALER_WALLET_Instance *twi,
                                 TALER_WALLET_MessageHandlerFn handler_f,
                                 void *handler_p);

/**
 * Set a handler for log messages from wallet-core.
 * Must be called before the wallet runs.
 *
 * Caution: The log message handler will be called from a different thread.
 */
void
TALER_WALLET_set_log_handler(struct TALER_WALLET_Instance *twi,
                             TALER_WALLET_LogHandlerFn handler_f,
                             void *handler_p);

/**
 * Set/override the JS file with the wallet-core implementation.
 * Must be called before the wallet runs.
 */
// FIXME: Not implemented!
//void
//TALER_WALLET_set_jsfile(struct TALER_WALLET_Instance *twi,
//                        const char *filename);

/**
 * Send a message to wallet-core.
 *
 * Responses will be sent asynchronously to the message handler
 * set with #TALER_WALLET_set_message_handler.
 */
int
TALER_WALLET_send_request(struct TALER_WALLET_Instance *twi,
                          const char *request);

/**
 * Run wallet-core in a thread.
 *
 * This function creates a new thread and returns immediately.
 *
 * Returns 0 on success or a non-zero error code otherwise.
 */
int
TALER_WALLET_run(struct TALER_WALLET_Instance *twi);

/**
 * Block until the wallet returns.
 */
void
TALER_WALLET_join(struct TALER_WALLET_Instance *twi);

/**
 * Destroy the wallet handle and free resources associated with it.
 *
 * Note that for a graceful shutdown of the wallet,
 * an appropriate shutdown message should be sent first,
 * and destroy() should only be called after the wallet has
 * sent a response to the shutdown message.
 */
//void
//TALER_WALLET_destroy(struct TALER_WALLET_Instance *twi);

/**
 * Handler for messages that should be logged.
 *
 * @param stream NOT YET IMPLEMENTED: indicator for the stream that
 *               the message is coming from,
 */
typedef void (*TALER_LogFn)(void *handler_p, int stream, const char *msg);

/**
 * Redirect stderr and stdout to a function.
 *
 * Workaround for platforms where stderr is not visible in logs.
 *
 * @return 0 on success, error code otherwise
 */
int
TALER_start_redirect_std(TALER_LogFn logfn, void *handler_p);

/**
 * Set the HTTP client implementation to be used by the wallet.
 *
 * @param twi wallet-core instance
 * @param impl HTTP client implementation
 */
void
TALER_set_http_client_implementation(struct TALER_WALLET_Instance *twi,
                                     struct JSHttpClientImplementation *impl);

/**
 * Set the reference CuRL-based HTTP client implementation as the one
 * to be used by the wallet.
 *
 * @param twi wallet-core instance
 */
void
TALER_set_curl_http_client(struct TALER_WALLET_Instance *twi);

#pragma mark -
/**
 * Build JSHttpClientImplementation struct for native HTTP client implementation to be used by the wallet.
 *
 * @param req_create HTTP client implementation
 * @param req_cancel HTTP client implementation
 * @param handler_p Pointer to Handler's "this"
 */
struct JSHttpClientImplementation *
TALER_pack_http_client_implementation(JSHttpReqCreateFn req_create,
                                      JSHttpReqCancelFn req_cancel,
                                      void *handler_p);

#endif /*_TALER_WALLET_LIB_H */
